*** Settings ***
Library        SeleniumLibrary
Resource    ../Resources/Common.robot
Resource    ../Resources/PageObjects/MainPage.robot
Resource    ../Resources/PageObjects/TopMenu.robot
Resource    ../Resources/PageObjects/PaintsPage.robot
Resource    ../Resources/PageObjects/AboutPage.robot
Resource    ../Resources/PageObjects/HowPage.robot
Resource    ../Resources/PageObjects/ShopPage.robot
Resource    ../Resources/PageObjects/ContactPage.robot
Resource    ../Resources/PageObjects/Cookies.robot
Resource    ../Resources/PageObjects/Amazon.robot
Resource    ../Resources/PageObjects/Allegro.robot

*** Keywords ***
Open Main Page PL
    [Arguments]    ${URL}
    MainPage.Verify Main Page In Polish Loaded

Open Paints Page PL
    [Arguments]    ${URL}
    TopMenu.Go To Paints Page
    PaintsPage.Verify Paints Page In Polish Loaded


Open About Page PL
    [Arguments]    ${URL}
    TopMenu.Go To About Page
    AboutPage.Verify About Page In Polish Loaded

Open How Page PL
    [Arguments]    ${URL}
    TopMenu.Go To How Page
    HowPage.Verify How Page In Polish Loaded

Open Shop Page PL
    [Arguments]    ${URL}
    TopMenu.Go To Shop Page
    ShopPage.Verify Shop Page In Polish Loaded

Open Contact Page PL
    [Arguments]    ${URL}
    TopMenu.Go To Contact Page
    ContactPage.Verify Contact Page In Polish Loaded

Open Main Page EN
    [Arguments]    ${URL}
    MainPage.Verify Main Page In English Loaded

Open Paints Page EN
    [Arguments]    ${URL}
    TopMenu.Go To Paints Page
    PaintsPage.Verify Paints Page In English Loaded


Open About Page EN
    [Arguments]    ${URL}
    TopMenu.Go To About Page
    AboutPage.Verify About Page In English Loaded

Open How Page EN
    [Arguments]    ${URL}
    TopMenu.Go To How Page
    HowPage.Verify How Page In English Loaded

Open Shop Page EN
    [Arguments]    ${URL}
    TopMenu.Go To Shop Page
    ShopPage.Verify Shop Page In English Loaded

Open Contact Page EN
    [Arguments]    ${URL}
    TopMenu.Go To Contact Page
    ContactPage.Verify Contact Page In English Loaded

# Accept All Cookies
#     Cookies.Accept All Cookies

Open PHTALO_GREEN Paint Details
    PaintsPage.Open PHTALO_GREEN Details popup
    PaintsPage.Close Paint Details popup
    Sleep    1s

Open PIROLLE_RED Paint Details
    PaintsPage.Open PIROLLE_RED Details popup
    PaintsPage.Close Paint Details popup
    Sleep    1s

Open BROWN_OXIDE Paint Details
    PaintsPage.Open BROWN_OXIDE Details popup
    PaintsPage.Close Paint Details popup
    Sleep    1s

Open TITANIUM_WHITE Paint Details
    PaintsPage.Open TITANIUM_WHITE Details popup
    PaintsPage.Close Paint Details popup
    Sleep    1s

Open YELLOW_OXIDE Paint Details
    PaintsPage.Open YELLOW_OXIDE Details popup
    PaintsPage.Close Paint Details popup
    Sleep    1s

Open PHTALO_BLUE Paint Details
    PaintsPage.Open PHTALO_BLUE Details popup
    PaintsPage.Close Paint Details popup
    Sleep    1s

Open MARS_RED Paint Details
    PaintsPage.Open MARS_RED Details popup
    PaintsPage.Close Paint Details popup
    Sleep    1s

Open WARM_GREEN Paint Details
    PaintsPage.Open WARM_GREEN Details popup
    PaintsPage.Close Paint Details popup
    Sleep    1s

Open ULTRAMARINE Paint Details
    PaintsPage.Open ULTRAMARINE Details popup
    PaintsPage.Close Paint Details popup
    Sleep    1s

Open PRIMARY_YELLOW Paint Details
    PaintsPage.Open PRIMARY_YELLOW Details popup
    PaintsPage.Close Paint Details popup
    Sleep    1s

Open MARS_BLACK Paint Details
    PaintsPage.Open MARS_BLACK Details popup
    PaintsPage.Close Paint Details popup
    Sleep    1s

Open VIOLET Paint Details
    PaintsPage.Open VIOLET Details popup
    PaintsPage.Close Paint Details popup
    Sleep    1s

Open Paints List In The Shop
    [Arguments]    ${URL}
    Open Shop Page PL    ${URL}
    ShopPage.Open Paints List In The Shop
    ShopPage.Verify Paints List In The Shop In Polish Loaded

Open Sets List In The Shop
    [Arguments]    ${URL}
    Open Shop Page PL    ${URL}
    ShopPage.Open Sets List In The Shop
    ShopPage.Verify Sets List In The Shop In Polish Loaded

Add One Paint Item To Cart
    ShopPage.Add Ultramarine Paint To Cart
    
Verify Shop Icon Navigates User To The Specific Paint Page From Paint Popup
    PaintsPage.Open PHTALO_GREEN Details popup
    PaintsPage.Click FMP Shop Icon
    ShopPage.Verify PHTALO_GREEN Shop Page Is Loaded
    Go To Previous Page
    PaintsPage.Close Paint Details popup
    Sleep    500ms

    PaintsPage.Open PIROLLE_RED Details popup
    PaintsPage.Click FMP Shop Icon
    ShopPage.Verify PIROLLE_RED Shop Page Is Loaded
    Go To Previous Page
    PaintsPage.Close Paint Details popup
    Sleep    500ms
    
    PaintsPage.Open BROWN_OXIDE Details popup
    PaintsPage.Click FMP Shop Icon
    ShopPage.Verify BROWN_OXIDE Shop Page Is Loaded
    Go To Previous Page
    PaintsPage.Close Paint Details popup
    Sleep    500ms

    PaintsPage.Open TITANIUM_WHITE Details popup
    PaintsPage.Click FMP Shop Icon
    ShopPage.Verify TITANIUM_WHITE Shop Page Is Loaded
    Go To Previous Page
    PaintsPage.Close Paint Details popup
    Sleep    500ms

    PaintsPage.Open YELLOW_OXIDE Details popup
    PaintsPage.Click FMP Shop Icon
    ShopPage.Verify YELLOW_OXIDE Shop Page Is Loaded
    Go To Previous Page
    PaintsPage.Close Paint Details popup
    Sleep    500ms

    PaintsPage.Open PHTALO_BLUE Details popup
    PaintsPage.Click FMP Shop Icon
    ShopPage.Verify PHTALO_BLUE Shop Page Is Loaded
    Go To Previous Page
    PaintsPage.Close Paint Details popup
    Sleep    500ms

    PaintsPage.Open MARS_RED Details popup
    PaintsPage.Click FMP Shop Icon
    ShopPage.Verify MARS_RED Shop Page Is Loaded
    Go To Previous Page
    PaintsPage.Close Paint Details popup
    Sleep    500ms

    PaintsPage.Open WARM_GREEN Details popup
    PaintsPage.Click FMP Shop Icon
    ShopPage.Verify WARM_GREEN Shop Page Is Loaded
    Go To Previous Page
    PaintsPage.Close Paint Details popup
    Sleep    500ms

    PaintsPage.Open ULTRAMARINE Details popup
    PaintsPage.Click FMP Shop Icon
    ShopPage.Verify ULTRAMARINE Shop Page Is Loaded
    Go To Previous Page
    PaintsPage.Close Paint Details popup
    Sleep    500ms

    PaintsPage.Open PRIMARY_YELLOW Details popup
    PaintsPage.Click FMP Shop Icon
    ShopPage.Verify PRIMARY_YELLOW Shop Page Is Loaded
    Go To Previous Page
    PaintsPage.Close Paint Details popup
    Sleep    500ms

    PaintsPage.Open MARS_BLACK Details popup
    PaintsPage.Click FMP Shop Icon
    ShopPage.Verify MARS_BLACK Shop Page Is Loaded
    Go To Previous Page
    PaintsPage.Close Paint Details popup
    Sleep    500ms

    PaintsPage.Open VIOLET Details popup
    PaintsPage.Click FMP Shop Icon
    ShopPage.Verify VIOLET Shop Page Is Loaded
    Go To Previous Page
    PaintsPage.Close Paint Details popup

Go To Previous Page

    Go Back

Verify User Navigates To The Specific Set Page After Clicking On Set Tile In Sets List
    ShopPage.Open SET_6 Page
    ShopPage.Verify SET_6 Shop Page Is Loaded
    Go To Previous Page
    ShopPage.Open SET_12 Page
    ShopPage.Verify SET_12 Shop Page Is Loaded
    Sleep    5ms

Verify Amazon Icon Navigates User To The Specific Amazon Page From Paint Popup
    PaintsPage.Open PHTALO_GREEN Details popup
    PaintsPage.Click Amazon Icon
    Amazon.Verify Amazon PHTALO_GREEN Page Loaded
    Go To Previous Page
    PaintsPage.Close Paint Details popup
    Sleep    500ms

    PaintsPage.Open PIROLLE_RED Details popup
    PaintsPage.Click Amazon Icon
    Amazon.Verify Amazon PIROLLE_RED Page Loaded
    Go To Previous Page
    PaintsPage.Close Paint Details popup
    Sleep    500ms
    
    PaintsPage.Open BROWN_OXIDE Details popup
    PaintsPage.Click Amazon Icon
    Amazon.Verify Amazon BROWN_OXIDE Page Loaded
    Go To Previous Page
    PaintsPage.Close Paint Details popup
    Sleep    500ms

    PaintsPage.Open TITANIUM_WHITE Details popup
    PaintsPage.Click Amazon Icon
    Amazon.Verify Amazon TITANIUM_WHITE Page Loaded
    Go To Previous Page
    PaintsPage.Close Paint Details popup
    Sleep    500ms

    PaintsPage.Open YELLOW_OXIDE Details popup
    PaintsPage.Click Amazon Icon
    Amazon.Verify Amazon YELLOW_OXIDE Page Loaded
    Go To Previous Page
    PaintsPage.Close Paint Details popup
    Sleep    500ms

    PaintsPage.Open PHTALO_BLUE Details popup
    PaintsPage.Click Amazon Icon
    Amazon.Verify Amazon PHTALO_BLUE Page Loaded
    Go To Previous Page
    PaintsPage.Close Paint Details popup
    Sleep    500ms

    PaintsPage.Open MARS_RED Details popup
    PaintsPage.Click Amazon Icon
    Amazon.Verify Amazon MARS_RED Page Loaded
    Go To Previous Page
    PaintsPage.Close Paint Details popup
    Sleep    500ms

    PaintsPage.Open WARM_GREEN Details popup
    PaintsPage.Click Amazon Icon
    Amazon.Verify Amazon WARM_GREEN Page Loaded
    Go To Previous Page
    PaintsPage.Close Paint Details popup
    Sleep    500ms

    PaintsPage.Open ULTRAMARINE Details popup
    PaintsPage.Click Amazon Icon
    Amazon.Verify Amazon ULTRAMARINE Page Loaded
    Go To Previous Page
    PaintsPage.Close Paint Details popup
    Sleep    500ms

    PaintsPage.Open PRIMARY_YELLOW Details popup
    PaintsPage.Click Amazon Icon
    Amazon.Verify Amazon PRIMARY_YELLOW Page Loaded
    Go To Previous Page
    PaintsPage.Close Paint Details popup
    Sleep    500ms

    PaintsPage.Open MARS_BLACK Details popup
    PaintsPage.Click Amazon Icon
    Amazon.Verify Amazon MARS_BLACK Page Loaded
    Go To Previous Page
    PaintsPage.Close Paint Details popup
    Sleep    500ms

    PaintsPage.Open VIOLET Details popup
    PaintsPage.Click Amazon Icon
    Amazon.Verify Amazon VIOLET Page Loaded
    Go To Previous Page
    PaintsPage.Close Paint Details popup

Verify Allegro Icon Navigates User To The Specific Allegro Page From Paint Popup
    PaintsPage.Open PHTALO_GREEN Details popup
    PaintsPage.Click Allegro Icon
    Allegro.Verify Allegro PHTALO_GREEN Page Loaded
    Go To Previous Page
    PaintsPage.Close Paint Details popup
    Sleep    500ms

    PaintsPage.Open PIROLLE_RED Details popup
    PaintsPage.Click Allegro Icon
    Allegro.Verify Allegro PIROLLE_RED Page Loaded
    Go To Previous Page
    PaintsPage.Close Paint Details popup
    Sleep    500ms
    
    PaintsPage.Open BROWN_OXIDE Details popup
    PaintsPage.Click Allegro Icon
    Allegro.Verify Allegro BROWN_OXIDE Page Loaded
    Go To Previous Page
    PaintsPage.Close Paint Details popup
    Sleep    500ms

    PaintsPage.Open TITANIUM_WHITE Details popup
    PaintsPage.Click Allegro Icon
    Allegro.Verify Allegro TITANIUM_WHITE Page Loaded
    Go To Previous Page
    PaintsPage.Close Paint Details popup
    Sleep    500ms

    PaintsPage.Open YELLOW_OXIDE Details popup
    PaintsPage.Click Allegro Icon
    Allegro.Verify Allegro YELLOW_OXIDE Page Loaded
    Go To Previous Page
    PaintsPage.Close Paint Details popup
    Sleep    500ms

    PaintsPage.Open PHTALO_BLUE Details popup
    PaintsPage.Click Allegro Icon
    Allegro.Verify Allegro PHTALO_BLUE Page Loaded
    Go To Previous Page
    PaintsPage.Close Paint Details popup
    Sleep    500ms

    PaintsPage.Open MARS_RED Details popup
    PaintsPage.Click Allegro Icon
    Allegro.Verify Allegro MARS_RED Page Loaded
    Go To Previous Page
    PaintsPage.Close Paint Details popup
    Sleep    500ms

    PaintsPage.Open WARM_GREEN Details popup
    PaintsPage.Click Allegro Icon
    Allegro.Verify Allegro WARM_GREEN Page Loaded
    Go To Previous Page
    PaintsPage.Close Paint Details popup
    Sleep    500ms

    PaintsPage.Open ULTRAMARINE Details popup
    PaintsPage.Click Allegro Icon
    Allegro.Verify Allegro ULTRAMARINE Page Loaded
    Go To Previous Page
    PaintsPage.Close Paint Details popup
    Sleep    500ms

    PaintsPage.Open PRIMARY_YELLOW Details popup
    PaintsPage.Click Allegro Icon
    Allegro.Verify Allegro PRIMARY_YELLOW Page Loaded
    Go To Previous Page
    PaintsPage.Close Paint Details popup
    Sleep    500ms

    PaintsPage.Open MARS_BLACK Details popup
    PaintsPage.Click Allegro Icon
    Allegro.Verify Allegro MARS_BLACK Page Loaded
    Go To Previous Page
    PaintsPage.Close Paint Details popup
    Sleep    500ms

    PaintsPage.Open VIOLET Details popup
    PaintsPage.Click Allegro Icon
    Allegro.Verify Allegro VIOLET Page Loaded
    Go To Previous Page
    PaintsPage.Close Paint Details popup