*** Settings ***
Library        SeleniumLibrary
Resource       PageObjects/TopMenu.robot
Resource       PageObjects/Cookies.robot

*** Variables ***

*** Keywords ***
Begin Test Suite
    Open Browser    about:blank     ${BROWSER}
    Maximize Browser Window
    Go To                           ${URL}
    Cookies.Accept All Cookies

End Test Suite
    Close Browser
