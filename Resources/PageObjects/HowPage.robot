*** Settings ***
Library        SeleniumLibrary
Library        String
Resource       TopMenu.robot
Resource       Footer.robot

*** Variables ***
${HOW}                        HOW
${STEP_ONE_IMAGE}             xpath=//*[@data-id='a3da2f7']//img[contains(@src, 'https://feelmypaint.com/wp-content/uploads/2024/02/Capture.png')]
${STEP_TWO_IMAGE}             xpath=//*[@data-id='fd65b94']//img[contains(@src, 'https://feelmypaint.com/wp-content/uploads/2024/02/Capture2.webp')]
${STEP_THREE_IMAGE}           xpath=//*[@data-id='c0771ed']//img[contains(@src, 'https://feelmypaint.com/wp-content/uploads/2024/02/Capture3.webp')]
${STEP_FOUR_IMAGE}            xpath=//*[@data-id='4cff758']//img[contains(@src, 'https://feelmypaint.com/wp-content/uploads/2024/02/Capture4.webp')]


*** Keywords ***
Verify How Page In Polish Loaded
    TopMenu.Verify Header In Polish Is Loaded
    Wait Until Page Contains                ${HOW}
    Wait Until Page Contains Element        ${STEP_ONE_IMAGE}
    Wait Until Page Contains Element        ${STEP_TWO_IMAGE}
    Wait Until Page Contains Element        ${STEP_THREE_IMAGE}
    Wait Until Page Contains Element        ${STEP_FOUR_IMAGE}
    
    #Header Text Verification
    @{expected_header_text}=    Create List     
    ...    Jak to robimy?
    ...    Proces produkcji naszej farby akrylowej składa się z kilku etapów

    ${actual_header_full}=    Get Text    xpath=//*[@data-id='3e5ae3a']
    ${actual_header_text}=    Split String    ${actual_header_full}    \n    # Splits the text into a list of paragraphs
    Should Be Equal As Strings    ${actual_header_text}    ${expected_header_text}

    #Step One Text Verification
    @{expected_step_one_text}=    Create List     
    ...    ETAP PIERWSZY
    ...    Pierwszym etapem jest przygotowanie składników: Farba akrylowa bazuje na dyspersji akrylowej, która jest wodnym roztworem specjalnych polimerów akrylowych. Dyspersja akrylowa dostarczana jest do nas w postaci gotowej w beczkach.
    ...    Farba zawiera również pigmenty, wypełniacze, stabilizatory i dodatki nadające farbie określone właściwości. Każdy składnik jest dokładnie ważony.

    ${actual_step_one_full}=    Get Text    xpath=//*[@data-id='a3da2f7']
    ${actual_step_one_text}=    Split String    ${actual_step_one_full}    \n    # Splits the text into a list of paragraphs
    Should Be Equal As Strings    ${actual_step_one_text}    ${expected_step_one_text}

    #Step Two Text Verification
    @{expected_step_two_text}=    Create List     
    ...    ETAP DRUGI
    ...    Drugi etap to wstępne mieszanie i zwilżanie: Dyspersja akrylowa jest mieszana z wodą i innymi składnikami w specjalnym zbiorniku, następnie dodajemy pigment, który określi kolor przyszłej farby, a także inne suche substancje, kontynuując mieszanie przy niskich prędkościach. Musimy zwilżyć każdą cząsteczkę pigmentu.

    ${actual_step_two_full}=    Get Text    xpath=//*[@data-id='fd65b94']
    ${actual_step_two_text}=    Split String    ${actual_step_two_full}    \n    # Splits the text into a list of paragraphs
    Should Be Equal As Strings    ${actual_step_two_text}    ${expected_step_two_text}

    #Step Three Text Verification
    @{expected_step_three_text}=    Create List     
    ...    TRZECI ETAP
    ...    Trzeci etap to dyspersja mieszaniny. Zwiększamy prędkość mieszania w celu równomiernego rozproszenia wszystkich substancji w farbie. W końcowej fazie wprowadzamy specjalne dodatki regulujące gęstość i utrzymujące świeżość farby.
    ...    Aby uzyskać gładką i jednorodną farbę, mielimy ją za pomocą młynów trójwalcowych. Młyn trójwalcowy to maszyna składająca się z trzech poziomych walców, które obracają się wokół własnej osi i mielą materiał pomiędzy nimi.
    ...    Po etapie z młynami trójwalcowymi pobieramy próbki farby w celu ostatecznej kontroli jakości przed jej zapakowaniem i przygotowaniem do wysyłki na rynek.

    ${actual_step_three_full}=    Get Text    xpath=//*[@data-id='c0771ed']
    ${actual_step_three_text}=    Split String    ${actual_step_three_full}    \n    # Splits the text into a list of paragraphs
    Should Be Equal As Strings    ${actual_step_three_text}    ${expected_step_three_text}

    #Step Four Text Verification
    @{expected_step_four_text}=    Create List     
    ...    CZWARTY ETAP
    ...    Etap pakowania. Jeśli wybrane próbki przejdą pozytywnie testy, przystępujemy do pakowania produktu. Gotowa farba jest wlewana do tubek, a następnie hermetycznie zamykana, aby zapobiec wysychaniu i rozlaniu.

    ${actual_step_four_full}=    Get Text    xpath=//*[@data-id='4cff758']
    ${actual_step_four_text}=    Split String    ${actual_step_four_full}    \n    # Splits the text into a list of paragraphs
    Should Be Equal As Strings    ${actual_step_four_text}    ${expected_step_four_text}

    Footer.Verify Footer In Polish Is Loaded