*** Settings ***
Library        SeleniumLibrary
Resource       TopMenu.robot
Resource       Footer.robot

*** Variables ***
${MAIN_PAGE_BANNER_IMAGE}         xpath=*//img[contains(@class, 'wp-image-1883')]
${OUR_PAINTS}                     NASZE FARBY
${HOW_WE_DO_IT}                   JAK TO ROBIMY?
${PAINTS_CARDS}                   xpath=*//div[contains(@data-element_type, 'container') and contains (@data-id, 'c17b7c7')]
${STORE_BUTTON}                   xpath=//span[contains(@class, 'elementor-button-text') and text()='SKLEP']
${PHTALO_GREEN_IMAGE}             xpath=//*[@data-id='15a2dd6']//img[contains(@src, 'https://feelmypaint.com/wp-content/uploads/2024/02/fmp_green.webp')]
${PIROLLE_RED_IMAGE}              xpath=//*[@data-id='617de84']//img[contains(@src, 'https://feelmypaint.com/wp-content/uploads/2024/02/fmp_red.webp')]
${BROWN_OXIDE_IMAGE}              xpath=//*[@data-id='b7dc318']//img[contains(@src, 'https://feelmypaint.com/wp-content/uploads/2024/03/fmp_brown-1')]
${TITANIUM_WHITE_IMAGE}           xpath=//*[@data-id='d616b7c']//img[contains(@src, 'https://feelmypaint.com/wp-content/uploads/2024/02/fmp_white.webp')]
${YELLOW_OXIDE_IMAGE}             xpath=//*[@data-id='bed5c02']//img[contains(@src, 'https://feelmypaint.com/wp-content/uploads/2023/11/fmp_yellow_shadow.jpg')]
${PHTALO_BLUE_IMAGE}              xpath=//*[@data-id='5631815']//img[contains(@src, 'https://feelmypaint.com/wp-content/uploads/2023/11/fmp_blue_shadow.jpg')]
${MARS_RED_IMAGE}                 xpath=//*[@data-id='c9de5e5']//img[contains(@src, 'https://feelmypaint.com/wp-content/uploads/2023/11/fmp_red_shadow.jpg')]
${WARM_GREEN_IMAGE}               xpath=//*[@data-id='17e1f7d']//img[contains(@src, 'https://feelmypaint.com/wp-content/uploads/2023/11/photo_2023-11-18_12-47-31.jpg')]
${ULTRAMARINE_IMAGE}              xpath=//*[@data-id='7518da5']//img[contains(@src, 'https://feelmypaint.com/wp-content/uploads/2024/02/fmp_blue.webp')]
${PRIMARY_YELLOW_IMAGE}           xpath=//*[@data-id='4de3d56']//img[contains(@src, 'https://feelmypaint.com/wp-content/uploads/2024/02/fmp_yellow.webp')]
${MARS_BLACK_IMAGE}               xpath=//*[@data-id='68d53dd']//img[contains(@src, 'https://feelmypaint.com/wp-content/uploads/2024/02/fmp_black.webp')]
${VIOLET_IMAGE}               xpath=//*[@data-id='17c333f']//img[contains(@src, 'https://feelmypaint.com/wp-content/uploads/2024/03/fmp_tube_jpeg_shadow_violet-2')]
${POPUP}                    xpath=//div[@class='dialog-widget dialog-lightbox-widget dialog-type-buttons dialog-type-lightbox elementor-popup-modal' and not(contains(@style, 'display: none'))]
${FMP_SHOP_ICON}            xpath=//img[contains(@src, 'https://feelmypaint.com/wp-content/uploads/2023/11/image_2023-11-29_16-35-48.png')]
${ALLEGRO_ICON}            xpath=//img[contains(@src, 'https://feelmypaint.com/wp-content/uploads/2023/11/46.jpg')]
${AMAZON_ICON}            xpath=//img[contains(@src, 'https://feelmypaint.com/wp-content/uploads/2023/11/amazon-de-neutral-02-123ecb3b416ce1d3.jpeg')]
${CLOSE_POPUP_BUTTON}        xpath=//a[@class='dialog-close-button dialog-lightbox-close-button' and not(contains(../../@style, 'display: none'))]


*** Keywords ***
Verify Paints Page In Polish Loaded
    TopMenu.Verify Header In Polish Is Loaded
    #Verify title is displayed
    ${Title_Info} =        Create List    FEEL    MY    ACRYLICS
    FOR    ${Title_Elements}    IN    @{Title_Info}
        Element Should Contain    //div[contains(@data-id, 'f418642')]    ${Title_Elements}
        
    END

    Wait Until Page Contains    LINIA BASIC

    #Verify paints description matches the image

    #PHTALO_GREEN
    ${PHTALO_GREEN_INFO} =            Create List   PG7    phtalo green    phthalocyanine zielona    +++ □
    FOR    ${Info}    IN    @{PHTALO_GREEN_INFO}
        Element Should Contain    //div[contains(@data-id, '15a2dd6')]    ${Info}
        
    END
    Page Should Contain Element            ${PHTALO_GREEN_IMAGE}
    
    #PIROLLE_RED
    ${PIROLLE_RED_INFO} =            Create List   PR254    pyrrole red    czerwień pirrolewa    +++ ◪
    FOR    ${Info}    IN    @{PIROLLE_RED_INFO}
        Element Should Contain    //div[contains(@data-id, '617de84')]    ${Info}
        
    END
    Page Should Contain Element            ${PIROLLE_RED_IMAGE}

    # BROWN_OXIDE
    ${BROWN_OXIDE_INFO} =            Create List   PBr6    brown iron oxide    brązowy tlenek żelaza    +++ ■
    FOR    ${Info}    IN    @{BROWN_OXIDE_INFO}
        Element Should Contain    //div[contains(@data-id, 'b7dc318')]    ${Info}
        
    END
    Page Should Contain Element            ${BROWN_OXIDE_IMAGE}

    #TITANIUM_WHITE
     ${TITANIUM_WHITE_INFO} =            Create List   PW6    titanium white    biel tytanowa    +++ ■
     FOR    ${Info}    IN    @{TITANIUM_WHITE_INFO}
         Element Should Contain    //div[contains(@data-id, 'd616b7c')]    ${Info}
        
     END
     Page Should Contain Element            ${TITANIUM_WHITE_IMAGE}

    #YELLOW_OXIDE
    ${YELLOW_OXIDE_INFO} =            Create List   PY42    yellow oxide    żólty tlenek    +++ ■
    FOR    ${Info}    IN    @{YELLOW_OXIDE_INFO}
        Element Should Contain    //div[contains(@data-id, 'bed5c02')]    ${Info}
        
    END
    Page Should Contain Element            ${YELLOW_OXIDE_IMAGE}

    #PHTALO_BLUE
    ${PHTALO_BLUE_INFO} =            Create List   PB15    phtalo blue    phthalocyanine niebeska    +++ □
    FOR    ${Info}    IN    @{PHTALO_BLUE_INFO}
        Element Should Contain    //div[contains(@data-id, '5631815')]    ${Info}
        
    END
    Page Should Contain Element            ${PHTALO_BLUE_IMAGE}

    #MARS_RED
    ${MARS_RED_INFO} =            Create List   PR101    mars red    czerwień zelazowa    +++ ■
    FOR    ${Info}    IN    @{MARS_RED_INFO}
        Element Should Contain    //div[contains(@data-id, 'c9de5e5')]    ${Info}
        
    END
    Page Should Contain Element            ${MARS_RED_IMAGE}

    #WARM_GREEN
    ${WARM_GREEN_INFO} =            Create List   PG7+PY74    warm green    czepła zeleń    +++ ◪
    FOR    ${Info}    IN    @{WARM_GREEN_INFO}
        Element Should Contain    //div[contains(@data-id, '17e1f7d')]    ${Info}
        
    END
    Page Should Contain Element            ${WARM_GREEN_IMAGE}

    #ULTRAMARINE
    ${ULTRAMARINE_INFO} =            Create List   PB29    ultramarine    ultramaryna    +++ □
    FOR    ${Info}    IN    @{ULTRAMARINE_INFO}
        Element Should Contain    //div[contains(@data-id, '7518da5')]    ${Info}
        
    END
    Page Should Contain Element            ${ULTRAMARINE_IMAGE}

    #PRIMARY_YELLOW
    ${PRIMARY_YELLOW_INFO} =            Create List   PY74    primary yellow    podstawowy żólty    +++ ◪
    FOR    ${Info}    IN    @{PRIMARY_YELLOW_INFO}
        Element Should Contain    //div[contains(@data-id, '4de3d56')]    ${Info}
        
    END
    Page Should Contain Element            ${PRIMARY_YELLOW_IMAGE}

    #MARS_BLACK
    ${MARS_BLACK_INFO} =            Create List   PBk11    mars black    czerń zelazowa    +++ ■
    FOR    ${Info}    IN    @{MARS_BLACK_INFO}
        Element Should Contain    //div[contains(@data-id, '68d53dd')]    ${Info}
        
    END
    Page Should Contain Element            ${MARS_BLACK_IMAGE}

    #VIOLET
    ${VIOLET_INFO} =            Create List   PW6+PV23    violet    fioletowa    +++ ■
    FOR    ${Info}    IN    @{VIOLET_INFO}
        Element Should Contain    //div[contains(@data-id, '17c333f')]    ${Info}
        
    END
    Page Should Contain Element            ${VIOLET_IMAGE}
    
    Footer.Verify Footer In Polish Is Loaded

Verify Paints Page In English Loaded
    TopMenu.Verify Header In English Is Loaded
    #Verify title is displayed
    ${Title_Info} =        Create List    FEEL    MY    ACRYLICS
    FOR    ${Title_Elements}    IN    @{Title_Info}
        Element Should Contain    //div[contains(@data-id, 'f418642')]    ${Title_Elements}
        
    END

    Wait Until Page Contains    BASIC LINE

    #Verify paints description matches the image

    #PHTALO_GREEN
    ${PHTALO_GREEN_INFO} =            Create List   PG7    phtalo green    phthalocyanine green    +++ □
    FOR    ${Info}    IN    @{PHTALO_GREEN_INFO}
        Element Should Contain    //div[contains(@data-id, '15a2dd6')]    ${Info}
        
    END
    Page Should Contain Element            ${PHTALO_GREEN_IMAGE}
    
    #PIROLLE_RED
    ${PIROLLE_RED_INFO} =            Create List   PR254    pyrrole red    czerwień pirrolewa    +++ ◪
    FOR    ${Info}    IN    @{PIROLLE_RED_INFO}
        Element Should Contain    //div[contains(@data-id, '617de84')]    ${Info}
        
    END
    Page Should Contain Element            ${PIROLLE_RED_IMAGE}

    # BROWN_OXIDE
    ${BROWN_OXIDE_INFO} =            Create List   PBr6    brown iron oxide    brązowy tlenek żelaza    +++ ■
    FOR    ${Info}    IN    @{BROWN_OXIDE_INFO}
        Element Should Contain    //div[contains(@data-id, 'b7dc318')]    ${Info}
        
    END
    Page Should Contain Element            ${BROWN_OXIDE_IMAGE}

    #TITANIUM_WHITE
     ${TITANIUM_WHITE_INFO} =            Create List   PW6    titanium white    biel tytanowa    +++ ■
     FOR    ${Info}    IN    @{TITANIUM_WHITE_INFO}
         Element Should Contain    //div[contains(@data-id, 'd616b7c')]    ${Info}
        
     END
     Page Should Contain Element            ${TITANIUM_WHITE_IMAGE}

    #YELLOW_OXIDE
    ${YELLOW_OXIDE_INFO} =            Create List   PY42    yellow oxide    żólty tlenek    +++ ■
    FOR    ${Info}    IN    @{YELLOW_OXIDE_INFO}
        Element Should Contain    //div[contains(@data-id, 'bed5c02')]    ${Info}
        
    END
    Page Should Contain Element            ${YELLOW_OXIDE_IMAGE}

    #PHTALO_BLUE
    ${PHTALO_BLUE_INFO} =            Create List   PB15    phtalo blue    phthalocyanine niebeska    +++ □
    FOR    ${Info}    IN    @{PHTALO_BLUE_INFO}
        Element Should Contain    //div[contains(@data-id, '5631815')]    ${Info}
        
    END
    Page Should Contain Element            ${PHTALO_BLUE_IMAGE}

    #MARS_RED
    ${MARS_RED_INFO} =            Create List   PR101    mars red    czerwień zelazowa    +++ ■
    FOR    ${Info}    IN    @{MARS_RED_INFO}
        Element Should Contain    //div[contains(@data-id, 'c9de5e5')]    ${Info}
        
    END
    Page Should Contain Element            ${MARS_RED_IMAGE}

    #WARM_GREEN
    ${WARM_GREEN_INFO} =            Create List   PG7+PY74    warm green    czepła zeleń    +++ ◪
    FOR    ${Info}    IN    @{WARM_GREEN_INFO}
        Element Should Contain    //div[contains(@data-id, '17e1f7d')]    ${Info}
        
    END
    Page Should Contain Element            ${WARM_GREEN_IMAGE}

    #ULTRAMARINE
    ${ULTRAMARINE_INFO} =            Create List   PB29    ultramarine    ultramaryna    +++ □
    FOR    ${Info}    IN    @{ULTRAMARINE_INFO}
        Element Should Contain    //div[contains(@data-id, '7518da5')]    ${Info}
        
    END
    Page Should Contain Element            ${ULTRAMARINE_IMAGE}

    #PRIMARY_YELLOW
    ${PRIMARY_YELLOW_INFO} =            Create List   PY74    primary yellow    podstawowy żólty    +++ ◪
    FOR    ${Info}    IN    @{PRIMARY_YELLOW_INFO}
        Element Should Contain    //div[contains(@data-id, '4de3d56')]    ${Info}
        
    END
    Page Should Contain Element            ${PRIMARY_YELLOW_IMAGE}

    #MARS_BLACK
    ${MARS_BLACK_INFO} =            Create List   PBk11    mars black    czerń zelazowa    +++ ■
    FOR    ${Info}    IN    @{MARS_BLACK_INFO}
        Element Should Contain    //div[contains(@data-id, '68d53dd')]    ${Info}
        
    END
    Page Should Contain Element            ${MARS_BLACK_IMAGE}

    #VIOLET
    ${VIOLET_INFO} =            Create List   PW6+PV23    violet    fioletowa    +++ ■
    FOR    ${Info}    IN    @{VIOLET_INFO}
        Element Should Contain    //div[contains(@data-id, '17c333f')]    ${Info}
        
    END
    Page Should Contain Element            ${VIOLET_IMAGE}
    
    Footer.Verify Footer Is Loaded

Open PHTALO_GREEN Details popup
    Click Element    ${PHTALO_GREEN_IMAGE}
    Wait Until Page Contains Element    ${POPUP}
    Wait Until Page Contains Element    ${FMP_SHOP_ICON}
    Wait Until Page Contains Element    ${ALLEGRO_ICON}
    Wait Until Page Contains Element    ${AMAZON_ICON}

    #PHTALO_GREEN Polish Text Verification
    @{expected_PHTALO_GREEN_description}=    Create List     
    ...    PHTALO GREEN
    ...    Odkryj wyjątkową jakość naszej artystycznej farby akrylowej z serii BASIC, zawierającej bogate pigmenty, które ożywią Twoje dzieła sztuki. Zapewniamy najwyższą satysfakcję z tej wszechstronnej kolekcji, odpowiedniej dla artystów na wszystkich poziomach zaawansowania. Odkryj nieskończone możliwości i pozwól swojej kreatywności rozkwitnąć dzięki serii BASIC – gdzie satysfakcja spotyka się z artystyczną doskonałością.
    ...    Seria: Basic
    ...    Pigment: PG7
    ...    Pokrycie: □ (przezroczysta)
    ...    Światłoodporność: +++
    ...    Pojemność: 120ml
    ...    Gdzie można kupić:

    ${actual_PHTALO_GREEN_description}=    Get Text    xpath=//*[@data-id='d5fd1db']
    ${actual_PHTALO_GREEN_description}=    Split String    ${actual_PHTALO_GREEN_description}    \n    # Splits the text into a list of paragraphs
    Should Be Equal As Strings    ${actual_PHTALO_GREEN_description}    ${expected_PHTALO_GREEN_description}

Open PIROLLE_RED Details popup
    Click Element    ${PIROLLE_RED_IMAGE}
    Wait Until Page Contains Element    ${POPUP}
    Wait Until Page Contains Element    ${FMP_SHOP_ICON}
    Wait Until Page Contains Element    ${ALLEGRO_ICON}
    Wait Until Page Contains Element    ${AMAZON_ICON}

    #PIROLLE_RED Polish Text Verification
    @{expected_PIROLLE_RED_description}=    Create List     
    ...    PYRROLE RED
    ...    Odkryj wyjątkową jakość naszej artystycznej farby akrylowej z serii BASIC, zawierającej bogate pigmenty, które ożywią Twoje dzieła sztuki. Zapewniamy najwyższą satysfakcję z tej wszechstronnej kolekcji, odpowiedniej dla artystów na wszystkich poziomach zaawansowania. Odkryj nieskończone możliwości i pozwól swojej kreatywności rozkwitnąć dzięki serii BASIC – gdzie satysfakcja spotyka się z artystyczną doskonałością.
    ...    Seria: Basic
    ...    Pigment: PR254
    ...    Pokrycie: ◪ (półprzeźroczysta)
    ...    Światłoodporność: +++
    ...    Pojemność: 120ml
    ...    Gdzie można kupić:

    ${actual_PIROLLE_RED_description}=    Get Text    xpath=//*[@data-id='d5fd1db']
    ${actual_PIROLLE_RED_description}=    Split String    ${actual_PIROLLE_RED_description}    \n    # Splits the text into a list of paragraphs
    Should Be Equal As Strings    ${actual_PIROLLE_RED_description}    ${expected_PIROLLE_RED_description}

Open BROWN_OXIDE Details popup
    Click Element    ${BROWN_OXIDE_IMAGE}
    Wait Until Page Contains Element    ${POPUP}
    Wait Until Page Contains Element    ${FMP_SHOP_ICON}
    Wait Until Page Contains Element    ${ALLEGRO_ICON}
    Wait Until Page Contains Element    ${AMAZON_ICON}

    #BROWN_OXIDE Polish Text Verification
    @{expected_BROWN_OXIDE_description}=    Create List     
    ...    BROWN OXIDE
    ...    Odkryj wyjątkową jakość naszej artystycznej farby akrylowej z serii BASIC, zawierającej bogate pigmenty, które ożywią Twoje dzieła sztuki. Zapewniamy najwyższą satysfakcję z tej wszechstronnej kolekcji, odpowiedniej dla artystów na wszystkich poziomach zaawansowania. Odkryj nieskończone możliwości i pozwól swojej kreatywności rozkwitnąć dzięki serii BASIC – gdzie satysfakcja spotyka się z artystyczną doskonałością.
    ...    Seria: Basic
    ...    Pigment: PBr6
    ...    Pokrycie: ■ (kryjąca)
    ...    Światłoodporność: +++
    ...    Pojemność: 120ml
    ...    Gdzie można kupić:

    ${actual_BROWN_OXIDE_description}=    Get Text    xpath=//*[@data-id='d5fd1db']
    ${actual_BROWN_OXIDE_description}=    Split String    ${actual_BROWN_OXIDE_description}    \n    # Splits the text into a list of paragraphs
    Should Be Equal As Strings    ${actual_BROWN_OXIDE_description}    ${expected_BROWN_OXIDE_description}

Open TITANIUM_WHITE Details popup
    Click Element    ${TITANIUM_WHITE_IMAGE}
    Wait Until Page Contains Element    ${POPUP}
    Wait Until Page Contains Element    ${FMP_SHOP_ICON}
    Wait Until Page Contains Element    ${ALLEGRO_ICON}
    Wait Until Page Contains Element    ${AMAZON_ICON}

    #TITANIUM_WHITE Polish Text Verification
    @{expected_PW6_description}=    Create List     
    ...    TYTANOWY BIAłY
    ...    Odkryj wyjątkową jakość naszej artystycznej farby akrylowej z serii BASIC, zawierającej bogate pigmenty, które ożywią Twoje dzieła sztuki. Zapewniamy najwyższą satysfakcję z tej wszechstronnej kolekcji, odpowiedniej dla artystów na wszystkich poziomach zaawansowania. Odkryj nieskończone możliwości i pozwól swojej kreatywności rozkwitnąć dzięki serii BASIC – gdzie satysfakcja spotyka się z artystyczną doskonałością.
    ...    Seria: Basic
    ...    Pigment: PW6
    ...    Pokrycie: ■ (kryjąca)
    ...    Światłoodporność: +++
    ...    Pojemność: 120ml
    ...    Gdzie można kupić:

    ${actual_PW6_description}=    Get Text    xpath=//*[@data-id='d5fd1db']
    ${actual_PW6_description}=    Split String    ${actual_PW6_description}    \n    # Splits the text into a list of paragraphs
    Should Be Equal As Strings    ${actual_PW6_description}    ${expected_PW6_description}

Open YELLOW_OXIDE Details popup
    Click Element    ${YELLOW_OXIDE_IMAGE}
    Wait Until Page Contains Element    ${POPUP}
    Wait Until Page Contains Element    ${FMP_SHOP_ICON}
    Wait Until Page Contains Element    ${ALLEGRO_ICON}
    Wait Until Page Contains Element    ${AMAZON_ICON}

    #YELLOW_OXIDE Polish Text Verification
    @{expected_YELLOW_OXIDE_description}=    Create List     
    ...    ŻÓŁTY TLENEK
    ...    Odkryj wyjątkową jakość naszej artystycznej farby akrylowej z serii BASIC, zawierającej bogate pigmenty, które ożywią Twoje dzieła sztuki. Zapewniamy najwyższą satysfakcję z tej wszechstronnej kolekcji, odpowiedniej dla artystów na wszystkich poziomach zaawansowania. Odkryj nieskończone możliwości i pozwól swojej kreatywności rozkwitnąć dzięki serii BASIC – gdzie satysfakcja spotyka się z artystyczną doskonałością.
    ...    Seria: Basic
    ...    Pigment: PY42
    ...    Pokrycie: ■ (kryjąca)
    ...    Światłoodporność: +++
    ...    Pojemność: 120ml
    ...    Gdzie można kupić:

    ${actual_YELLOW_OXIDE_description}=    Get Text    xpath=//*[@data-id='60d6915']
    ${actual_YELLOW_OXIDE_description}=    Split String    ${actual_YELLOW_OXIDE_description}    \n    # Splits the text into a list of paragraphs
    Should Be Equal As Strings    ${actual_YELLOW_OXIDE_description}    ${expected_YELLOW_OXIDE_description}

Open PHTALO_BLUE Details popup
    Click Element    ${PHTALO_BLUE_IMAGE}
    Wait Until Page Contains Element    ${POPUP}
    Wait Until Page Contains Element    ${FMP_SHOP_ICON}
    Wait Until Page Contains Element    ${ALLEGRO_ICON}
    Wait Until Page Contains Element    ${AMAZON_ICON}

    #PHTALO_BLUE Polish Text Verification
    @{expected_PHTALO_BLUE_description}=    Create List     
    ...    PHTALO NIEBIESKI
    ...    Odkryj wyjątkową jakość naszej artystycznej farby akrylowej z serii BASIC, zawierającej bogate pigmenty, które ożywią Twoje dzieła sztuki. Zapewniamy najwyższą satysfakcję z tej wszechstronnej kolekcji, odpowiedniej dla artystów na wszystkich poziomach zaawansowania. Odkryj nieskończone możliwości i pozwól swojej kreatywności rozkwitnąć dzięki serii BASIC – gdzie satysfakcja spotyka się z artystyczną doskonałością.
    ...    Seria: Basic
    ...    Pigment: PB15
    ...    Pokrycie: □ (przezroczysta)
    ...    Światłoodporność: +++
    ...    Pojemność: 120ml
    ...    Gdzie można kupić:

    ${actual_PHTALO_BLUE_description}=    Get Text    xpath=//*[@data-id='d5fd1db']
    ${actual_PHTALO_BLUE_description}=    Split String    ${actual_PHTALO_BLUE_description}    \n    # Splits the text into a list of paragraphs
    Should Be Equal As Strings    ${actual_PHTALO_BLUE_description}    ${expected_PHTALO_BLUE_description}

Open MARS_RED Details popup
    Click Element    ${MARS_RED_IMAGE}
    Wait Until Page Contains Element    ${POPUP}
    Wait Until Page Contains Element    ${FMP_SHOP_ICON}
    Wait Until Page Contains Element    ${ALLEGRO_ICON}
    Wait Until Page Contains Element    ${AMAZON_ICON}

    #MARS_RED Polish Text Verification
    @{expected_MARS_RED_description}=    Create List     
    ...    CZERWONY TLENEK
    ...    Odkryj wyjątkową jakość naszej artystycznej farby akrylowej z serii BASIC, zawierającej bogate pigmenty, które ożywią Twoje dzieła sztuki. Zapewniamy najwyższą satysfakcję z tej wszechstronnej kolekcji, odpowiedniej dla artystów na wszystkich poziomach zaawansowania. Odkryj nieskończone możliwości i pozwól swojej kreatywności rozkwitnąć dzięki serii BASIC – gdzie satysfakcja spotyka się z artystyczną doskonałością.
    ...    Seria: Basic
    ...    Pigment: PR101
    ...    Pokrycie: ■ (kryjąca)
    ...    Światłoodporność: +++
    ...    Pojemność: 120ml
    ...    Gdzie można kupić:

    ${actual_MARS_RED_description}=    Get Text    xpath=//*[@data-id='d5fd1db']
    ${actual_MARS_RED_description}=    Split String    ${actual_MARS_RED_description}    \n    # Splits the text into a list of paragraphs
    Should Be Equal As Strings    ${actual_MARS_RED_description}    ${expected_MARS_RED_description}

Open WARM_GREEN Details popup
    Click Element    ${WARM_GREEN_IMAGE}
    Wait Until Page Contains Element    ${POPUP}
    Wait Until Page Contains Element    ${FMP_SHOP_ICON}
    Wait Until Page Contains Element    ${ALLEGRO_ICON}
    Wait Until Page Contains Element    ${AMAZON_ICON}

    #WARM_GREEN Polish Text Verification
    @{expected_WARM_GREEN_description}=    Create List     
    ...    CIEPŁA ZIELEŃ
    ...    Odkryj wyjątkową jakość naszej artystycznej farby akrylowej z serii BASIC, zawierającej bogate pigmenty, które ożywią Twoje dzieła sztuki. Zapewniamy najwyższą satysfakcję z tej wszechstronnej kolekcji, odpowiedniej dla artystów na wszystkich poziomach zaawansowania. Odkryj nieskończone możliwości i pozwól swojej kreatywności rozkwitnąć dzięki serii BASIC – gdzie satysfakcja spotyka się z artystyczną doskonałością.
    ...    Seria: Basic
    ...    Pigment: PG7 + PY74
    ...    Pokrycie: ◪ (półprzeźroczysta)
    ...    Światłoodporność: +++
    ...    Pojemność: 120ml
    ...    Gdzie można kupić:

    ${actual_WARM_GREEN_description}=    Get Text    xpath=//*[@data-id='d5fd1db']
    ${actual_WARM_GREEN_description}=    Split String    ${actual_WARM_GREEN_description}    \n    # Splits the text into a list of paragraphs
    Should Be Equal As Strings    ${actual_WARM_GREEN_description}    ${expected_WARM_GREEN_description}

Open ULTRAMARINE Details popup
    Click Element    ${ULTRAMARINE_IMAGE}
    Wait Until Page Contains Element    ${POPUP}
    Wait Until Page Contains Element    ${FMP_SHOP_ICON}
    Wait Until Page Contains Element    ${ALLEGRO_ICON}
    Wait Until Page Contains Element    ${AMAZON_ICON}

    #ULTRAMARINE Polish Text Verification
    @{expected_ULTRAMARINE_description}=    Create List     
    ...    ULTRAMARYNA
    ...    Odkryj wyjątkową jakość naszej artystycznej farby akrylowej z serii BASIC, zawierającej bogate pigmenty, które ożywią Twoje dzieła sztuki. Zapewniamy najwyższą satysfakcję z tej wszechstronnej kolekcji, odpowiedniej dla artystów na wszystkich poziomach zaawansowania. Odkryj nieskończone możliwości i pozwól swojej kreatywności rozkwitnąć dzięki serii BASIC – gdzie satysfakcja spotyka się z artystyczną doskonałością.
    ...    Seria: Basic
    ...    Pigment: PB29
    ...    Pokrycie: □ (przezroczysta)
    ...    Światłoodporność: +++
    ...    Pojemność: 120ml
    ...    Gdzie można kupić:

    ${actual_ULTRAMARINE_description}=    Get Text    xpath=//*[@data-id='d5fd1db']
    ${actual_ULTRAMARINE_description}=    Split String    ${actual_ULTRAMARINE_description}    \n    # Splits the text into a list of paragraphs
    Should Be Equal As Strings    ${actual_ULTRAMARINE_description}    ${expected_ULTRAMARINE_description}

Open PRIMARY_YELLOW Details popup
    Click Element    ${PRIMARY_YELLOW_IMAGE}
    Wait Until Page Contains Element    ${POPUP}
    Wait Until Page Contains Element    ${FMP_SHOP_ICON}
    Wait Until Page Contains Element    ${ALLEGRO_ICON}
    Wait Until Page Contains Element    ${AMAZON_ICON}

    #PRIMARY_YELLOW Polish Text Verification
    @{expected_PRIMARY_YELLOW_description}=    Create List     
    ...    PODSTAWOWY ŻÓŁTY
    ...    Odkryj wyjątkową jakość naszej artystycznej farby akrylowej z serii BASIC, zawierającej bogate pigmenty, które ożywią Twoje dzieła sztuki. Zapewniamy najwyższą satysfakcję z tej wszechstronnej kolekcji, odpowiedniej dla artystów na wszystkich poziomach zaawansowania. Odkryj nieskończone możliwości i pozwól swojej kreatywności rozkwitnąć dzięki serii BASIC – gdzie satysfakcja spotyka się z artystyczną doskonałością.
    ...    Seria: Basic
    ...    Pigment: PY74
    ...    Pokrycie: ◪ (półprzeźroczysta)
    ...    Światłoodporność: +++
    ...    Pojemność: 120ml
    ...    Gdzie można kupić:

    ${actual_PRIMARY_YELLOW_description}=    Get Text    xpath=//*[@data-id='d5fd1db']
    ${actual_PRIMARY_YELLOW_description}=    Split String    ${actual_PRIMARY_YELLOW_description}    \n    # Splits the text into a list of paragraphs
    Should Be Equal As Strings    ${actual_PRIMARY_YELLOW_description}    ${expected_PRIMARY_YELLOW_description}

Open MARS_BLACK Details popup
    Click Element    ${MARS_BLACK_IMAGE}
    Wait Until Page Contains Element    ${POPUP}
    Wait Until Page Contains Element    ${FMP_SHOP_ICON}
    Wait Until Page Contains Element    ${ALLEGRO_ICON}
    Wait Until Page Contains Element    ${AMAZON_ICON}

    #MARS_BLACK Polish Text Verification
    @{expected_MARS_BLACK_description}=    Create List     
    ...    CZARNY MARS
    ...    Odkryj wyjątkową jakość naszej artystycznej farby akrylowej z serii BASIC, zawierającej bogate pigmenty, które ożywią Twoje dzieła sztuki. Zapewniamy najwyższą satysfakcję z tej wszechstronnej kolekcji, odpowiedniej dla artystów na wszystkich poziomach zaawansowania. Odkryj nieskończone możliwości i pozwól swojej kreatywności rozkwitnąć dzięki serii BASIC – gdzie satysfakcja spotyka się z artystyczną doskonałością.
    ...    Seria: Basic
    ...    Pigment: PBk11
    ...    Pokrycie: ■ (kryjąca)
    ...    Światłoodporność: +++
    ...    Pojemność: 120ml
    ...    Gdzie można kupić:

    ${actual_MARS_BLACK_description}=    Get Text    xpath=//*[@data-id='d5fd1db']
    ${actual_MARS_BLACK_description}=    Split String    ${actual_MARS_BLACK_description}    \n    # Splits the text into a list of paragraphs
    Should Be Equal As Strings    ${actual_MARS_BLACK_description}    ${expected_MARS_BLACK_description}

Open VIOLET Details popup
    Click Element    ${VIOLET_IMAGE}
    Wait Until Page Contains Element    ${POPUP}
    Wait Until Page Contains Element    ${FMP_SHOP_ICON}
    Wait Until Page Contains Element    ${ALLEGRO_ICON}
    Wait Until Page Contains Element    ${AMAZON_ICON}

    #VIOLET Polish Text Verification
    @{expected_VIOLET_description}=    Create List     
    ...    FIOLETOWY
    ...    Odkryj wyjątkową jakość naszej artystycznej farby akrylowej z serii BASIC, zawierającej bogate pigmenty, które ożywią Twoje dzieła sztuki. Zapewniamy najwyższą satysfakcję z tej wszechstronnej kolekcji, odpowiedniej dla artystów na wszystkich poziomach zaawansowania. Odkryj nieskończone możliwości i pozwól swojej kreatywności rozkwitnąć dzięki serii BASIC – gdzie satysfakcja spotyka się z artystyczną doskonałością.
    ...    Seria: Basic
    ...    Pigment: PW6+PV23
    ...    Pokrycie: ■ (kryjąca)
    ...    Światłoodporność: +++
    ...    Pojemność: 120ml
    ...    Gdzie można kupić:

    ${actual_VIOLET_description}=    Get Text    xpath=//*[@data-id='d5fd1db']
    ${actual_VIOLET_description}=    Split String    ${actual_VIOLET_description}    \n    # Splits the text into a list of paragraphs
    Should Be Equal As Strings    ${actual_VIOLET_description}    ${expected_VIOLET_description}

Close Paint Details popup
    Wait Until Page Contains Element    ${CLOSE_POPUP_BUTTON}
    Click Element    ${CLOSE_POPUP_BUTTON}

Click FMP Shop Icon
    Click Element    ${FMP_SHOP_ICON}

Click Amazon Icon
    Click Element    ${AMAZON_ICON}

Click Allegro Icon
    Click Element    ${ALLEGRO_ICON}