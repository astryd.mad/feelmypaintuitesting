*** Settings ***
Library        SeleniumLibrary

*** Variables ***
${PHTALO_GREEN_ALLEGRO_URL_ID}         14073383934
${PIROLLE_RED_ALLEGRO_URL_ID}         14073355033
${BROWN_OXIDE_ALLEGRO_URL_ID}         14073042324
${TITANIUM_WHITE_ALLEGRO_URL_ID}         14073347847
${YELLOW_OXIDE_ALLEGRO_URL_ID}         14482070180
${PHTALO_BLUE_ALLEGRO_URL_ID}         14482024128
${MARS_RED_ALLEGRO_URL_ID}         14482093629
${WARM_GREEN_ALLEGRO_URL_ID}         14481972385
${ULTRAMARINE_ALLEGRO_URL_ID}         14059615991
${PRIMARY_YELLOW_ALLEGRO_URL_ID}         14073363119
${MARS_BLACK_ALLEGRO_URL_ID}         14073093537
${VIOLET_ALLEGRO_URL_ID}         15242333694


*** Keywords ***
Verify Allegro PHTALO_GREEN Page Loaded
    ${CURRENT_URL}=  Get Location
    Should Contain  ${CURRENT_URL}  ${PHTALO_GREEN_ALLEGRO_URL_ID}

Verify Allegro PIROLLE_RED Page Loaded
    ${CURRENT_URL}=  Get Location
    Should Contain  ${CURRENT_URL}  ${PIROLLE_RED_ALLEGRO_URL_ID}

Verify Allegro BROWN_OXIDE Page Loaded
    ${CURRENT_URL}=  Get Location
    Should Contain  ${CURRENT_URL}  ${BROWN_OXIDE_ALLEGRO_URL_ID}

Verify Allegro TITANIUM_WHITE Page Loaded
    ${CURRENT_URL}=  Get Location
    Should Contain  ${CURRENT_URL}  ${TITANIUM_WHITE_ALLEGRO_URL_ID}

Verify Allegro YELLOW_OXIDE Page Loaded
    ${CURRENT_URL}=  Get Location
    Should Contain  ${CURRENT_URL}  ${YELLOW_OXIDE_ALLEGRO_URL_ID}

Verify Allegro PHTALO_BLUE Page Loaded
    ${CURRENT_URL}=  Get Location
    Should Contain  ${CURRENT_URL}  ${PHTALO_BLUE_ALLEGRO_URL_ID}

Verify Allegro MARS_RED Page Loaded
    ${CURRENT_URL}=  Get Location
    Should Contain  ${CURRENT_URL}  ${MARS_RED_ALLEGRO_URL_ID}

Verify Allegro WARM_GREEN Page Loaded
    ${CURRENT_URL}=  Get Location
    Should Contain  ${CURRENT_URL}  ${WARM_GREEN_ALLEGRO_URL_ID}

Verify Allegro ULTRAMARINE Page Loaded
    ${CURRENT_URL}=  Get Location
    Should Contain  ${CURRENT_URL}  ${ULTRAMARINE_ALLEGRO_URL_ID}

Verify Allegro PRIMARY_YELLOW Page Loaded
    ${CURRENT_URL}=  Get Location
    Should Contain  ${CURRENT_URL}  ${PRIMARY_YELLOW_ALLEGRO_URL_ID}

Verify Allegro MARS_BLACK Page Loaded
    ${CURRENT_URL}=  Get Location
    Should Contain  ${CURRENT_URL}  ${MARS_BLACK_ALLEGRO_URL_ID}

Verify Allegro VIOLET Page Loaded
    ${CURRENT_URL}=  Get Location
    Should Contain  ${CURRENT_URL}  ${VIOLET_ALLEGRO_URL_ID}
