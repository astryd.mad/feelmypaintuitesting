*** Settings ***
Library        SeleniumLibrary
Library        String
Resource       TopMenu.robot
Resource       Footer.robot

*** Variables ***
${SHOP_PAGE_BANNER}                xpath=//*[@data-id='bf6cbea']
${DELIVERY_ICON}                   xpath=//*[@data-id='04910e8']//img[contains(@src, 'https://feelmypaint.com/wp-content/uploads/2023/11/aboutus-icons-1.png')]
${RETURN_ICON}                     xpath=//*[@data-id='b2fb446']//img[contains(@src, 'https://feelmypaint.com/wp-content/uploads/2023/11/aboutus-icons-2.png')]
${FROM_SELLER_ICON}                xpath=//*[@data-id='d77c784']//img[contains(@src, 'https://feelmypaint.com/wp-content/uploads/2023/11/aboutus-icons-3.png')]
${SINGLE_PAINT_ITEM_HEADER}        xpath=//*[@data-id='4909e82']//h2[contains(text(), 'Farby Akrylowe 120ml')]
${SINGLE_PAINT_ITEM_IMAGE}         xpath=//*[@data-id='4909e82']//img[contains(@src, 'https://feelmypaint.com/wp-content/uploads/2023/11/fmp_white-1')]
${SETS_HEADER}                     xpath=//*[@data-id='8ffc047']//h2[contains(text(), 'Zestawy Farb Akrylowych')]
${SETS_IMAGE}                      xpath=//*[@data-id='8ffc047']//img[contains(@src, 'https://feelmypaint.com/wp-content/uploads/2024/03/fmp_sets')]
${RECENTLY_ADDED_HEADER}           Ostatnio dodane produkty
${RECENTLY_ADDED_TILES}            xpath=//*[@data-id='3d2d67c']//ul/li
${RECENTLY_ADDED_TILES_COUNT}      4
${ALL_PRODUCTS_IN_CATEGORY}        Ostatnio dodane produkty
${PAINTS_LIST_SHOP}               Wszystkie produkty z kategorii
@{SHOP_LIST_PAINT_IMG}             'https://feelmypaint.com/produkt/farba-akrylowa-feelmypaint-biel-tytanowa-pw6120ml/'    'https://feelmypaint.com/produkt/feelmypaint-farba-akrylowa-brazowa-pbr7120ml/'    'https://feelmypaint.com/produkt/farba-akrylowa-feelmypaint-ciepla-zielen-pg7py74-120ml/'    'https://feelmypaint.com/produkt/farba-akrylowa-feelmypaint-czarny-mars-pbk11120ml/'    'https://feelmypaint.com/produkt/farba-akrylowa-czerwien-pirolowa-pr254-120ml/'    'https://feelmypaint.com/produkt/farba-akrylowa-feelmypaint-czerwony-tlenek-pr-101-120ml/'    'https://feelmypaint.com/produkt/farba-akrylowa-feelmypaint-fioletowa-pw6pv23-120ml/'    'https://feelmypaint.com/produkt/farba-akrylowa-ftalocyjanina-niebeska-pb15-120ml/'    'https://feelmypaint.com/produkt/farba-akrylowa-ftalocyjanina-zielona-pg7-120ml/'    'https://feelmypaint.com/produkt/farba-akrylowafarba-akrylowa/'    'https://feelmypaint.com/produkt/farba-akrylowa-feelmypaint-zolta-py74-120ml/'    'https://feelmypaint.com/produkt/farba-akrylowa-zolty-tlenek-py42-120ml/'    'https://feelmypaint.com/produkt/farba-akrylowa-zolty-tlenek-py42-120ml/'
@{SHOP_LIST_SET_IMG}        'https://feelmypaint.com/produkt/twoj-wlasny-zestaw/'    'https://feelmypaint.com/produkt/zestaw-farb-akrylowych-6x120ml/'    'https://feelmypaint.com/produkt/zestaw-farb-akrylowych-feelmypaint-11-x-120-ml-1320-ml/'   
@{CURRENT_PRICE}       '15,00'
@{CURRENT_CURRENCY}    'zł'
@{PAINT_TITLE}         Farba Akrylowa FeelMyPaint Biel Tytanowa PW6,120ml    Farba Akrylowa FeelMyPaint Brązowa PBr6,120ml    Farba Akrylowa FeelMyPaint Ciepła Zieleń PG7+PY74, 120ml    Farba Akrylowa FeelMyPaint Czarny Mars PBk11,120ml    Farba Akrylowa FeelMyPaint Czerwień Pirolowa PR254, 120ml    Farba Akrylowa FeelMyPaint Czerwony Tlenek PR101, 120ml    Farba Akrylowa FeelMyPaint Fioletowa PW6+PV23, 120ml    Farba Akrylowa FeelMyPaint Ftalocyjanina Niebieska PB15, 120ml    Farba Akrylowa FeelMyPaint Ftalocyjanina Zielona PG7 120ml    Farba Akrylowa FeelMyPaint Ultramaryna PB29, 120ml    Farba Akrylowa FeelMyPaint Żółta PY74, 120ml    Farba Akrylowa FeelMyPaint Żółty Tlenek PY42, 120ml
@{SET_TITLE}            Twój własny zestaw 6 kolorów!    Zestaw farb akrylowych 6x120ml    Zestaw farb akrylowych FeelMyPaint 12×120 ml, 1440ml
@{ADD_TO_CART_TEXT}    'Dodaj do koszyka'
@{SET_PRICE}        '79,00'    '141,00'
@{CHOOSE_OPTION_TEXT}    'Wybierz Opcję'


*** Keywords ***
Verify Shop Page In Polish Loaded
    TopMenu.Verify Header In Polish Is Loaded
    Wait Until Page Contains Element        ${SHOP_PAGE_BANNER}
    Wait Until Page Contains Element        ${DELIVERY_ICON}
    Wait Until Page Contains Element        ${RETURN_ICON}
    Wait Until Page Contains Element        ${FROM_SELLER_ICON}
    
    #Delivery Text Verification
    @{expected_delivery_text}=    Create List     
    ...    Ekspresowa wysyłka
    ...    Ekspresowa wysyłka Mnóstwo opcji dostawy, wysyłka nawet w 24h!

    ${actual_delivery_full}=    Get Text    xpath=//*[@data-id='04910e8']
    ${actual_delivery_text}=    Split String    ${actual_delivery_full}    \n    # Splits the text into a list of paragraphs
    Should Be Equal As Strings    ${actual_delivery_text}    ${expected_delivery_text}

    #Return Text Verification
    @{expected_return_text}=    Create List     
    ...    Reklamacje i zwroty
    ...    Reklamacje i zwroty Rozpatrujemy i realizujemy szybko i bezpłatnie

    ${actual_return_full}=    Get Text    xpath=//*[@data-id='b2fb446']
    ${actual_return_text}=    Split String    ${actual_return_full}    \n    # Splits the text into a list of paragraphs
    Should Be Equal As Strings    ${actual_return_text}    ${expected_return_text}

    #From Seller Text Verification
    @{expected_from_seller_text}=    Create List     
    ...    OD PRODUCENTA
    ...    Jesteśmy producentem. Kupuj bezpośrednio!

    ${actual_from_seller_full}=    Get Text    xpath=//*[@data-id='d77c784']
    ${actual_from_seller_text}=    Split String    ${actual_from_seller_full}    \n    # Splits the text into a list of paragraphs
    Should Be Equal As Strings    ${actual_from_seller_text}    ${expected_from_seller_text}

    Wait Until Page Contains Element        ${SINGLE_PAINT_ITEM_HEADER}
    Wait Until Page Contains Element        ${SINGLE_PAINT_ITEM_IMAGE}
    Wait Until Page Contains Element        ${SETS_HEADER}
    Wait Until Page Contains Element        ${SETS_IMAGE}
    
    Wait Until Page Contains                ${RECENTLY_ADDED_HEADER}
    #Recently added products tiles verification
    ${element_count}=    Get Element Count    ${RECENTLY_ADDED_TILES}
    #should be equals to 4 but now it has a bug
    Should Be Equal As Integers    ${element_count}    ${RECENTLY_ADDED_TILES_COUNT}

    Footer.Verify Footer In Polish Is Loaded

Open Paints List In The Shop
    Click Element    ${SINGLE_PAINT_ITEM_IMAGE}

Open Sets List In The Shop
    Click Element    ${SETS_HEADER}

Verify Paints List In The Shop In Polish Loaded
    TopMenu.Verify Header In Polish Is Loaded
    #Verify title is displayed
   
    Wait Until Page Contains    Wszystkie produkty z kategorii

    #Verify paints title and price matches the image, "Add to cart" is displayed
   
    #TITANIUM_WHITE
    Page Should Contain Element    xpath=//img[contains(@src, 'https://feelmypaint.com/wp-content/uploads/2023/11/fmp_white-1')]
    Page Should Contain Element    xpath=//*[@href=${SHOP_LIST_PAINT_IMG}[0]]//*[contains(text(), '${PAINT_TITLE}[0]')]
    Page Should Contain Element    xpath=//*[@href=${SHOP_LIST_PAINT_IMG}[0]]//*[@class='price']//*[contains(text(), ${CURRENT_PRICE}[0])]
    Page Should Contain Element    xpath=//*[contains(@data-product_id, '541') and contains(text(), ${ADD_TO_CART_TEXT}[0])]

    #BROWN_OXIDE
    Page Should Contain Element    xpath=//img[contains(@src, 'https://feelmypaint.com/wp-content/uploads/2024/03/fmp_brown-1')]
    Page Should Contain Element    xpath=//*[@href=${SHOP_LIST_PAINT_IMG}[1]]//*[contains(text(), '${PAINT_TITLE}[1]')]
    Page Should Contain Element    xpath=//*[@href=${SHOP_LIST_PAINT_IMG}[1]]//*[@class='price']//*[contains(text(), ${CURRENT_PRICE}[0])]
    Page Should Contain Element    xpath=//*[contains(@data-product_id, '539') and contains(text(), ${ADD_TO_CART_TEXT}[0])]

    #WARM_GREEN
    Page Should Contain Element    xpath=//img[contains(@src, 'https://feelmypaint.com/wp-content/uploads/2023/11/fmp_green-1')]
    Page Should Contain Element    xpath=//*[@href=${SHOP_LIST_PAINT_IMG}[2]]//*[contains(text(), '${PAINT_TITLE}[2]')]
    Page Should Contain Element    xpath=//*[@href=${SHOP_LIST_PAINT_IMG}[2]]//*[@class='price']//*[contains(text(), ${CURRENT_PRICE}[0])]
    Page Should Contain Element    xpath=//*[contains(@data-product_id, '546') and contains(text(), ${ADD_TO_CART_TEXT}[0])]

    #MARS_BLACK
    Page Should Contain Element    xpath=//img[contains(@src, 'https://feelmypaint.com/wp-content/uploads/2023/11/fmp_black')]
    Page Should Contain Element    xpath=//*[@href=${SHOP_LIST_PAINT_IMG}[3]]//*[contains(text(), '${PAINT_TITLE}[3]')]
    Page Should Contain Element    xpath=//*[@href=${SHOP_LIST_PAINT_IMG}[3]]//*[@class='price']//*[contains(text(), ${CURRENT_PRICE}[0])]
    Page Should Contain Element    xpath=//*[contains(@data-product_id, '540') and contains(text(), ${ADD_TO_CART_TEXT}[0])]

    #PIROLLE_RED
    Page Should Contain Element    xpath=//img[contains(@src, 'https://feelmypaint.com/wp-content/uploads/2023/11/fmp_red')]
    Page Should Contain Element    xpath=//*[@href=${SHOP_LIST_PAINT_IMG}[4]]//*[contains(text(), '${PAINT_TITLE}[4]')]
    Page Should Contain Element    xpath=//*[@href=${SHOP_LIST_PAINT_IMG}[4]]//*[@class='price']//*[contains(text(), ${CURRENT_PRICE}[0])]
    Page Should Contain Element    xpath=//*[contains(@data-product_id, '541') and contains(text(), ${ADD_TO_CART_TEXT}[0])]

    #MARS_RED
    Page Should Contain Element    xpath=//img[contains(@src, 'https://feelmypaint.com/wp-content/uploads/2023/11/fmp_red_shadow')]
    Page Should Contain Element    xpath=//*[@href=${SHOP_LIST_PAINT_IMG}[5]]//*[contains(text(), '${PAINT_TITLE}[5]')]
    Page Should Contain Element    xpath=//*[@href=${SHOP_LIST_PAINT_IMG}[5]]//*[@class='price']//*[contains(text(), ${CURRENT_PRICE}[0])]
    Page Should Contain Element    xpath=//*[contains(@data-product_id, '552') and contains(text(), ${ADD_TO_CART_TEXT}[0])]

    #VIOLET
    Page Should Contain Element    xpath=//img[contains(@src, 'https://feelmypaint.com/wp-content/uploads/2024/03/fmp_tube_jpeg_shadow_violet')]
    Page Should Contain Element    xpath=//*[@href=${SHOP_LIST_PAINT_IMG}[6]]//*[contains(text(), '${PAINT_TITLE}[6]')]
    Page Should Contain Element    xpath=//*[@href=${SHOP_LIST_PAINT_IMG}[6]]//*[@class='price']//*[contains(text(), ${CURRENT_PRICE}[0])]
    Page Should Contain Element    xpath=//*[contains(@data-product_id, '2050') and contains(text(), ${ADD_TO_CART_TEXT}[0])]

    #PHTALO_BLUE
    Page Should Contain Element    xpath=//img[contains(@src, 'https://feelmypaint.com/wp-content/uploads/2023/11/fmp_blue_shadow')]
    Page Should Contain Element    xpath=//*[@href=${SHOP_LIST_PAINT_IMG}[7]]//*[contains(text(), '${PAINT_TITLE}[7]')]
    Page Should Contain Element    xpath=//*[@href=${SHOP_LIST_PAINT_IMG}[7]]//*[@class='price']//*[contains(text(), ${CURRENT_PRICE}[0])]
    Page Should Contain Element    xpath=//*[contains(@data-product_id, '548') and contains(text(), ${ADD_TO_CART_TEXT}[0])]

    #PHTALO_GREEN
    Page Should Contain Element    xpath=//img[contains(@src, 'https://feelmypaint.com/wp-content/uploads/2023/11/fmp_green')]
    Page Should Contain Element    xpath=//*[@href=${SHOP_LIST_PAINT_IMG}[8]]//*[contains(text(), '${PAINT_TITLE}[8]')]
    Page Should Contain Element    xpath=//*[@href=${SHOP_LIST_PAINT_IMG}[8]]//*[@class='price']//*[contains(text(), ${CURRENT_PRICE}[0])]
    Page Should Contain Element    xpath=//*[contains(@data-product_id, '545') and contains(text(), ${ADD_TO_CART_TEXT}[0])]

    #ULTRAMARINE
    Page Should Contain Element    xpath=//img[contains(@src, 'https://feelmypaint.com/wp-content/uploads/2023/11/ultramarine')]
    Page Should Contain Element    xpath=//*[@href=${SHOP_LIST_PAINT_IMG}[9]]//*[contains(text(), '${PAINT_TITLE}[9]')]
    Page Should Contain Element    xpath=//*[@href=${SHOP_LIST_PAINT_IMG}[9]]//*[@class='price']//*[contains(text(), ${CURRENT_PRICE}[0])]
    Page Should Contain Element    xpath=//*[contains(@data-product_id, '520') and contains(text(), ${ADD_TO_CART_TEXT}[0])]

    #PRIMARY_YELLOW
    Page Should Contain Element    xpath=//img[contains(@src, 'https://feelmypaint.com/wp-content/uploads/2023/11/fmp_yellow')]
    Page Should Contain Element    xpath=//*[@href=${SHOP_LIST_PAINT_IMG}[10]]//*[contains(text(), '${PAINT_TITLE}[10]')]
    Page Should Contain Element    xpath=//*[@href=${SHOP_LIST_PAINT_IMG}[10]]//*[@class='price']//*[contains(text(), ${CURRENT_PRICE}[0])]
    Page Should Contain Element    xpath=//*[contains(@data-product_id, '544') and contains(text(), ${ADD_TO_CART_TEXT}[0])]

    #YELLOW_OXIDE
    Page Should Contain Element    xpath=//img[contains(@src, 'https://feelmypaint.com/wp-content/uploads/2023/11/fmp_yellow_shadow')]
    Page Should Contain Element    xpath=//*[@href=${SHOP_LIST_PAINT_IMG}[11]]//*[contains(text(), '${PAINT_TITLE}[11]')]
    Page Should Contain Element    xpath=//*[@href=${SHOP_LIST_PAINT_IMG}[11]]//*[@class='price']//*[contains(text(), ${CURRENT_PRICE}[0])]
    Page Should Contain Element    xpath=//*[contains(@data-product_id, '550') and contains(text(), ${ADD_TO_CART_TEXT}[0])]

    Footer.Verify Footer In Polish Is Loaded

Verify Sets List In The Shop In Polish Loaded
    TopMenu.Verify Header In Polish Is Loaded
    #Verify title is displayed
   
    Wait Until Page Contains    Wszystkie produkty z kategorii

    #Verify paints title and price matches the image, "Add to cart" is displayed
   
    #OWN_SET
    Page Should Contain Element    xpath=//img[contains(@src, 'https://feelmypaint.com/wp-content/uploads/2024/02/NPP_8114')]
    Page Should Contain Element    xpath=//*[@href=${SHOP_LIST_SET_IMG}[0]]//*[contains(text(), '${SET_TITLE}[0]')]
    Page Should Contain Element    xpath=//*[@href=${SHOP_LIST_SET_IMG}[0]]//*[@class='price']//*[contains(text(), ${SET_PRICE}[0])]
    Page Should Contain Element    xpath=//*[contains(@data-product_id, '664') and contains(text(), 'Wybierz opcję')]
    Page Should Contain Element    xpath=//*[@href=${SHOP_LIST_SET_IMG}[0]]//*[contains(text(), ${CURRENT_CURRENCY}[0])]

    #SET_6
    Page Should Contain Element    xpath=//img[contains(@src, 'https://feelmypaint.com/wp-content/uploads/2024/02/NPP_8173')]
    Page Should Contain Element    xpath=//*[@href=${SHOP_LIST_SET_IMG}[1]]//*[contains(text(), '${SET_TITLE}[1]')]
    Page Should Contain Element    xpath=//*[@href=${SHOP_LIST_SET_IMG}[1]]//*[@class='price']//*[contains(text(), ${SET_PRICE}[0])]
    Page Should Contain Element    xpath=//*[contains(@data-product_id, '2006') and contains(text(), ${ADD_TO_CART_TEXT}[0])]
    Page Should Contain Element    xpath=//*[@href=${SHOP_LIST_SET_IMG}[1]]//*[contains(text(), ${CURRENT_CURRENCY}[0])]

    #SET_12
    Page Should Contain Element    xpath=//img[contains(@src, 'https://feelmypaint.com/wp-content/uploads/2024/03/fmp_sets')]
    Page Should Contain Element    xpath=//*[@href=${SHOP_LIST_SET_IMG}[2]]//*[contains(text(), '${SET_TITLE}[2]')]
    Page Should Contain Element    xpath=//*[@href=${SHOP_LIST_SET_IMG}[2]]//*[@class='price']//*[contains(text(), ${SET_PRICE}[1])]
    Page Should Contain Element    xpath=//*[contains(@data-product_id, '555') and contains(text(), ${ADD_TO_CART_TEXT}[0])]
    Page Should Contain Element    xpath=//*[@href=${SHOP_LIST_SET_IMG}[2]]//*[contains(text(), ${CURRENT_CURRENCY}[0])]

    Footer.Verify Footer In Polish Is Loaded

Add Ultramarine Paint To Cart
    Click Element    xpath=//*[contains(@data-product_id, '520') and contains(text(), 'Dodaj do koszyka')]
    Sleep    10s
    Wait Until Page Contains Element    xpath=//*[@id='elementor-menu-cart__toggle_button']//*[contains(text(), '15,00')]
    Wait Until Page Contains Element    xpath=//*[@data-counter='1']
    Wait Until Page Contains Element    xpath=//*[contains(@class, 'post-520')]//*[contains(text(), 'Zobacz koszyk')]

Verify TITANIUM_WHITE Shop Page Is Loaded
    TopMenu.Verify Header In Polish Is Loaded

    #Header Text Verification
    Wait Until Page Contains    ${PAINT_TITLE}[0]

    @{expected_description_text}=    Create List     
    ...    Nasza podstawowa gama kolorów to:
    ...    11 żywych kolorów: odpowiednie dla początkujących lub jako dodatek do istniejącej kolekcji artysty. Każdy kolor jest projektowany indywidualnie, aby zapewnić optymalną ilość pigmentu dla każdej receptury.
    ...    Wysokiej jakości pigmenty zapewniają równomierne, nasycone kolory i doskonałe krycie, dzięki czemu kolory nie blakną z upływem czasu. Wszystkie używane przez nas pigmenty mają bardzo dobrą lub doskonałą odporność na światło.
    ...    Duże tubki o pojemności 120 ml są wystarczająco duże, aby pomieścić wystarczającą ilość farby do większości projektów lub obrazów
    ...    Spoiwo składa się w 100% z żywicy akrylowej. Farbę można łatwo rozcieńczyć wodą
    ...    Nasze farby są nietoksyczne i bezpieczne.
    ...    Jesteśmy producentem i z radością przyjmiemy opinie oraz sugestie dotyczące tworzenia nowych i ulepszania istniejących produktów.

    ${actual_description_full}=    Get Text    xpath=//*[@data-id='5bee1109']
    ${actual_description_text}=    Split String    ${actual_description_full}    \n    # Splits the text into a list of paragraphs
    Should Be Equal As Strings    ${actual_description_text}    ${expected_description_text}

    Wait Until Page Contains Element    xpath=//*[@data-id='31d45dce']//*[contains(text(), ${CURRENT_PRICE}[0])]
    Wait Until Page Contains Element    xpath=//*[@data-id='31d45dce']//*[contains(text(), ${CURRENT_CURRENCY}[0])]
    Wait Until Page Contains Element    xpath=//*[@name='add-to-cart' and contains(text(), ${ADD_TO_CART_TEXT}[0])]

    Footer.Verify Footer In Polish Is Loaded

Verify PHTALO_GREEN Shop Page Is Loaded
    TopMenu.Verify Header In Polish Is Loaded

    #Header Text Verification
    Wait Until Page Contains    ${PAINT_TITLE}[8]

    @{expected_description_text}=    Create List     
    ...    Nasza podstawowa gama kolorów to:
    ...    11 żywych kolorów: odpowiednie dla początkujących lub jako dodatek do istniejącej kolekcji artysty. Każdy kolor jest projektowany indywidualnie, aby zapewnić optymalną ilość pigmentu dla każdej receptury.
    ...    Wysokiej jakości pigmenty zapewniają równomierne, nasycone kolory i doskonałe krycie, dzięki czemu kolory nie blakną z upływem czasu. Wszystkie używane przez nas pigmenty mają bardzo dobrą lub doskonałą odporność na światło.
    ...    Duże tubki o pojemności 120 ml są wystarczająco duże, aby pomieścić wystarczającą ilość farby do większości projektów lub obrazów
    ...    Spoiwo składa się w 100% z żywicy akrylowej. Farbę można łatwo rozcieńczyć wodą
    ...    Nasze farby są nietoksyczne i bezpieczne.
    ...    Jesteśmy producentem i z radością przyjmiemy opinie oraz sugestie dotyczące tworzenia nowych i ulepszania istniejących produktów.

    ${actual_description_full}=    Get Text    xpath=//*[@data-id='5bee1109']
    ${actual_description_text}=    Split String    ${actual_description_full}    \n    # Splits the text into a list of paragraphs
    Should Be Equal As Strings    ${actual_description_text}    ${expected_description_text}

    Wait Until Page Contains Element    xpath=//*[@data-id='31d45dce']//*[contains(text(), ${CURRENT_PRICE}[0])]
    Wait Until Page Contains Element    xpath=//*[@data-id='31d45dce']//*[contains(text(), ${CURRENT_CURRENCY}[0])]
    Wait Until Page Contains Element    xpath=//*[@name='add-to-cart' and contains(text(), ${ADD_TO_CART_TEXT}[0])]

    Footer.Verify Footer In Polish Is Loaded
    
Verify PIROLLE_RED Shop Page Is Loaded
    TopMenu.Verify Header In Polish Is Loaded

    #Header Text Verification
    Wait Until Page Contains    ${PAINT_TITLE}[4]

    @{expected_description_text}=    Create List     
    ...    Nasza podstawowa gama kolorów to:
    ...    11 żywych kolorów: odpowiednie dla początkujących lub jako dodatek do istniejącej kolekcji artysty. Każdy kolor jest projektowany indywidualnie, aby zapewnić optymalną ilość pigmentu dla każdej receptury.
    ...    Wysokiej jakości pigmenty zapewniają równomierne, nasycone kolory i doskonałe krycie, dzięki czemu kolory nie blakną z upływem czasu. Wszystkie używane przez nas pigmenty mają bardzo dobrą lub doskonałą odporność na światło.
    ...    Duże tubki o pojemności 120 ml są wystarczająco duże, aby pomieścić wystarczającą ilość farby do większości projektów lub obrazów
    ...    Spoiwo składa się w 100% z żywicy akrylowej. Farbę można łatwo rozcieńczyć wodą
    ...    Nasze farby są nietoksyczne i bezpieczne.
    ...    Jesteśmy producentem i z radością przyjmiemy opinie oraz sugestie dotyczące tworzenia nowych i ulepszania istniejących produktów.

    ${actual_description_full}=    Get Text    xpath=//*[@data-id='5bee1109']
    ${actual_description_text}=    Split String    ${actual_description_full}    \n    # Splits the text into a list of paragraphs
    Should Be Equal As Strings    ${actual_description_text}    ${expected_description_text}

    Wait Until Page Contains Element    xpath=//*[@data-id='31d45dce']//*[contains(text(), ${CURRENT_PRICE}[0])]
    Wait Until Page Contains Element    xpath=//*[@data-id='31d45dce']//*[contains(text(), ${CURRENT_CURRENCY}[0])]
    Wait Until Page Contains Element    xpath=//*[@name='add-to-cart' and contains(text(), ${ADD_TO_CART_TEXT}[0])]

    Footer.Verify Footer In Polish Is Loaded

Verify BROWN_OXIDE Shop Page Is Loaded
    TopMenu.Verify Header In Polish Is Loaded

    #Header Text Verification
    Wait Until Page Contains    ${PAINT_TITLE}[1]

    @{expected_description_text}=    Create List     
    ...    Nasza podstawowa gama kolorów to:
    ...    11 żywych kolorów: odpowiednie dla początkujących lub jako dodatek do istniejącej kolekcji artysty. Każdy kolor jest projektowany indywidualnie, aby zapewnić optymalną ilość pigmentu dla każdej receptury.
    ...    Wysokiej jakości pigmenty zapewniają równomierne, nasycone kolory i doskonałe krycie, dzięki czemu kolory nie blakną z upływem czasu. Wszystkie używane przez nas pigmenty mają bardzo dobrą lub doskonałą odporność na światło.
    ...    Duże tubki o pojemności 120 ml są wystarczająco duże, aby pomieścić wystarczającą ilość farby do większości projektów lub obrazów
    ...    Spoiwo składa się w 100% z żywicy akrylowej. Farbę można łatwo rozcieńczyć wodą
    ...    Nasze farby są nietoksyczne i bezpieczne.
    ...    Jesteśmy producentem i z radością przyjmiemy opinie oraz sugestie dotyczące tworzenia nowych i ulepszania istniejących produktów.

    ${actual_description_full}=    Get Text    xpath=//*[@data-id='5bee1109']
    ${actual_description_text}=    Split String    ${actual_description_full}    \n    # Splits the text into a list of paragraphs
    Should Be Equal As Strings    ${actual_description_text}    ${expected_description_text}

    Wait Until Page Contains Element    xpath=//*[@data-id='31d45dce']//*[contains(text(), ${CURRENT_PRICE}[0])]
    Wait Until Page Contains Element    xpath=//*[@data-id='31d45dce']//*[contains(text(), ${CURRENT_CURRENCY}[0])]
    Wait Until Page Contains Element    xpath=//*[@name='add-to-cart' and contains(text(), ${ADD_TO_CART_TEXT}[0])]

    Footer.Verify Footer In Polish Is Loaded

Verify YELLOW_OXIDE Shop Page Is Loaded
    TopMenu.Verify Header In Polish Is Loaded

    #Header Text Verification
    Wait Until Page Contains    ${PAINT_TITLE}[11]

    @{expected_description_text}=    Create List     
    ...    Nasza podstawowa gama kolorów to:
    ...    11 żywych kolorów: odpowiednie dla początkujących lub jako dodatek do istniejącej kolekcji artysty. Każdy kolor jest projektowany indywidualnie, aby zapewnić optymalną ilość pigmentu dla każdej receptury.
    ...    Wysokiej jakości pigmenty zapewniają równomierne, nasycone kolory i doskonałe krycie, dzięki czemu kolory nie blakną z upływem czasu. Wszystkie używane przez nas pigmenty mają bardzo dobrą lub doskonałą odporność na światło.
    ...    Duże tubki o pojemności 120 ml są wystarczająco duże, aby pomieścić wystarczającą ilość farby do większości projektów lub obrazów
    ...    Spoiwo składa się w 100% z żywicy akrylowej. Farbę można łatwo rozcieńczyć wodą
    ...    Nasze farby są nietoksyczne i bezpieczne.
    ...    Jesteśmy producentem i z radością przyjmiemy opinie oraz sugestie dotyczące tworzenia nowych i ulepszania istniejących produktów.

    ${actual_description_full}=    Get Text    xpath=//*[@data-id='5bee1109']
    ${actual_description_text}=    Split String    ${actual_description_full}    \n    # Splits the text into a list of paragraphs
    Should Be Equal As Strings    ${actual_description_text}    ${expected_description_text}

    Wait Until Page Contains Element    xpath=//*[@data-id='31d45dce']//*[contains(text(), ${CURRENT_PRICE}[0])]
    Wait Until Page Contains Element    xpath=//*[@data-id='31d45dce']//*[contains(text(), ${CURRENT_CURRENCY}[0])]
    Wait Until Page Contains Element    xpath=//*[@name='add-to-cart' and contains(text(), ${ADD_TO_CART_TEXT}[0])]

    Footer.Verify Footer In Polish Is Loaded

Verify PHTALO_BLUE Shop Page Is Loaded
    TopMenu.Verify Header In Polish Is Loaded

    #Header Text Verification
    Wait Until Page Contains    ${PAINT_TITLE}[7]

    @{expected_description_text}=    Create List     
    ...    Nasza podstawowa gama kolorów to:
    ...    11 żywych kolorów: odpowiednie dla początkujących lub jako dodatek do istniejącej kolekcji artysty. Każdy kolor jest projektowany indywidualnie, aby zapewnić optymalną ilość pigmentu dla każdej receptury.
    ...    Wysokiej jakości pigmenty zapewniają równomierne, nasycone kolory i doskonałe krycie, dzięki czemu kolory nie blakną z upływem czasu. Wszystkie używane przez nas pigmenty mają bardzo dobrą lub doskonałą odporność na światło.
    ...    Duże tubki o pojemności 120 ml są wystarczająco duże, aby pomieścić wystarczającą ilość farby do większości projektów lub obrazów
    ...    Spoiwo składa się w 100% z żywicy akrylowej. Farbę można łatwo rozcieńczyć wodą
    ...    Nasze farby są nietoksyczne i bezpieczne.
    ...    Jesteśmy producentem i z radością przyjmiemy opinie oraz sugestie dotyczące tworzenia nowych i ulepszania istniejących produktów.

    ${actual_description_full}=    Get Text    xpath=//*[@data-id='5bee1109']
    ${actual_description_text}=    Split String    ${actual_description_full}    \n    # Splits the text into a list of paragraphs
    Should Be Equal As Strings    ${actual_description_text}    ${expected_description_text}

    Wait Until Page Contains Element    xpath=//*[@data-id='31d45dce']//*[contains(text(), ${CURRENT_PRICE}[0])]
    Wait Until Page Contains Element    xpath=//*[@data-id='31d45dce']//*[contains(text(), ${CURRENT_CURRENCY}[0])]
    Wait Until Page Contains Element    xpath=//*[@name='add-to-cart' and contains(text(), ${ADD_TO_CART_TEXT}[0])]

    Footer.Verify Footer In Polish Is Loaded

Verify MARS_RED Shop Page Is Loaded
    TopMenu.Verify Header In Polish Is Loaded

    #Header Text Verification
    Wait Until Page Contains    ${PAINT_TITLE}[5]

    @{expected_description_text}=    Create List     
    ...    Nasza podstawowa gama kolorów to:
    ...    11 żywych kolorów: odpowiednie dla początkujących lub jako dodatek do istniejącej kolekcji artysty. Każdy kolor jest projektowany indywidualnie, aby zapewnić optymalną ilość pigmentu dla każdej receptury.
    ...    Wysokiej jakości pigmenty zapewniają równomierne, nasycone kolory i doskonałe krycie, dzięki czemu kolory nie blakną z upływem czasu. Wszystkie używane przez nas pigmenty mają bardzo dobrą lub doskonałą odporność na światło.
    ...    Duże tubki o pojemności 120 ml są wystarczająco duże, aby pomieścić wystarczającą ilość farby do większości projektów lub obrazów
    ...    Spoiwo składa się w 100% z żywicy akrylowej. Farbę można łatwo rozcieńczyć wodą
    ...    Nasze farby są nietoksyczne i bezpieczne.
    ...    Jesteśmy producentem i z radością przyjmiemy opinie oraz sugestie dotyczące tworzenia nowych i ulepszania istniejących produktów.

    ${actual_description_full}=    Get Text    xpath=//*[@data-id='5bee1109']
    ${actual_description_text}=    Split String    ${actual_description_full}    \n    # Splits the text into a list of paragraphs
    Should Be Equal As Strings    ${actual_description_text}    ${expected_description_text}

    Wait Until Page Contains Element    xpath=//*[@data-id='31d45dce']//*[contains(text(), ${CURRENT_PRICE}[0])]
    Wait Until Page Contains Element    xpath=//*[@data-id='31d45dce']//*[contains(text(), ${CURRENT_CURRENCY}[0])]
    Wait Until Page Contains Element    xpath=//*[@name='add-to-cart' and contains(text(), ${ADD_TO_CART_TEXT}[0])]

    Footer.Verify Footer In Polish Is Loaded

Verify WARM_GREEN Shop Page Is Loaded
    TopMenu.Verify Header In Polish Is Loaded

    #Header Text Verification
    Wait Until Page Contains    ${PAINT_TITLE}[2]

    @{expected_description_text}=    Create List     
    ...    Nasza podstawowa gama kolorów to:
    ...    11 żywych kolorów: odpowiednie dla początkujących lub jako dodatek do istniejącej kolekcji artysty. Każdy kolor jest projektowany indywidualnie, aby zapewnić optymalną ilość pigmentu dla każdej receptury.
    ...    Wysokiej jakości pigmenty zapewniają równomierne, nasycone kolory i doskonałe krycie, dzięki czemu kolory nie blakną z upływem czasu. Wszystkie używane przez nas pigmenty mają bardzo dobrą lub doskonałą odporność na światło.
    ...    Duże tubki o pojemności 120 ml są wystarczająco duże, aby pomieścić wystarczającą ilość farby do większości projektów lub obrazów
    ...    Spoiwo składa się w 100% z żywicy akrylowej. Farbę można łatwo rozcieńczyć wodą
    ...    Nasze farby są nietoksyczne i bezpieczne.
    ...    Jesteśmy producentem i z radością przyjmiemy opinie oraz sugestie dotyczące tworzenia nowych i ulepszania istniejących produktów.

    ${actual_description_full}=    Get Text    xpath=//*[@data-id='5bee1109']
    ${actual_description_text}=    Split String    ${actual_description_full}    \n    # Splits the text into a list of paragraphs
    Should Be Equal As Strings    ${actual_description_text}    ${expected_description_text}

    Wait Until Page Contains Element    xpath=//*[@data-id='31d45dce']//*[contains(text(), ${CURRENT_PRICE}[0])]
    Wait Until Page Contains Element    xpath=//*[@data-id='31d45dce']//*[contains(text(), ${CURRENT_CURRENCY}[0])]
    Wait Until Page Contains Element    xpath=//*[@name='add-to-cart' and contains(text(), ${ADD_TO_CART_TEXT}[0])]

    Footer.Verify Footer In Polish Is Loaded

Verify ULTRAMARINE Shop Page Is Loaded
    TopMenu.Verify Header In Polish Is Loaded

    #Header Text Verification
    Wait Until Page Contains    ${PAINT_TITLE}[9]

    @{expected_description_text}=    Create List     
    ...    Nasza podstawowa gama kolorów to:
    ...    11 żywych kolorów: odpowiednie dla początkujących lub jako dodatek do istniejącej kolekcji artysty. Każdy kolor jest projektowany indywidualnie, aby zapewnić optymalną ilość pigmentu dla każdej receptury.
    ...    Wysokiej jakości pigmenty zapewniają równomierne, nasycone kolory i doskonałe krycie, dzięki czemu kolory nie blakną z upływem czasu. Wszystkie używane przez nas pigmenty mają bardzo dobrą lub doskonałą odporność na światło.
    ...    Duże tubki o pojemności 120 ml są wystarczająco duże, aby pomieścić wystarczającą ilość farby do większości projektów lub obrazów
    ...    Spoiwo składa się w 100% z żywicy akrylowej. Farbę można łatwo rozcieńczyć wodą
    ...    Nasze farby są nietoksyczne i bezpieczne.
    ...    Jesteśmy producentem i z radością przyjmiemy opinie oraz sugestie dotyczące tworzenia nowych i ulepszania istniejących produktów.

    ${actual_description_full}=    Get Text    xpath=//*[@data-id='5bee1109']
    ${actual_description_text}=    Split String    ${actual_description_full}    \n    # Splits the text into a list of paragraphs
    Should Be Equal As Strings    ${actual_description_text}    ${expected_description_text}

    Wait Until Page Contains Element    xpath=//*[@data-id='31d45dce']//*[contains(text(), ${CURRENT_PRICE}[0])]
    Wait Until Page Contains Element    xpath=//*[@data-id='31d45dce']//*[contains(text(), ${CURRENT_CURRENCY}[0])]
    Wait Until Page Contains Element    xpath=//*[@name='add-to-cart' and contains(text(), ${ADD_TO_CART_TEXT}[0])]

    Footer.Verify Footer In Polish Is Loaded

Verify PRIMARY_YELLOW Shop Page Is Loaded
    TopMenu.Verify Header In Polish Is Loaded

    #Header Text Verification
    Wait Until Page Contains    ${PAINT_TITLE}[10]

    @{expected_description_text}=    Create List     
    ...    Nasza podstawowa gama kolorów to:
    ...    11 żywych kolorów: odpowiednie dla początkujących lub jako dodatek do istniejącej kolekcji artysty. Każdy kolor jest projektowany indywidualnie, aby zapewnić optymalną ilość pigmentu dla każdej receptury.
    ...    Wysokiej jakości pigmenty zapewniają równomierne, nasycone kolory i doskonałe krycie, dzięki czemu kolory nie blakną z upływem czasu. Wszystkie używane przez nas pigmenty mają bardzo dobrą lub doskonałą odporność na światło.
    ...    Duże tubki o pojemności 120 ml są wystarczająco duże, aby pomieścić wystarczającą ilość farby do większości projektów lub obrazów
    ...    Spoiwo składa się w 100% z żywicy akrylowej. Farbę można łatwo rozcieńczyć wodą
    ...    Nasze farby są nietoksyczne i bezpieczne.
    ...    Jesteśmy producentem i z radością przyjmiemy opinie oraz sugestie dotyczące tworzenia nowych i ulepszania istniejących produktów.

    ${actual_description_full}=    Get Text    xpath=//*[@data-id='5bee1109']
    ${actual_description_text}=    Split String    ${actual_description_full}    \n    # Splits the text into a list of paragraphs
    Should Be Equal As Strings    ${actual_description_text}    ${expected_description_text}

    Wait Until Page Contains Element    xpath=//*[@data-id='31d45dce']//*[contains(text(), ${CURRENT_PRICE}[0])]
    Wait Until Page Contains Element    xpath=//*[@data-id='31d45dce']//*[contains(text(), ${CURRENT_CURRENCY}[0])]
    Wait Until Page Contains Element    xpath=//*[@name='add-to-cart' and contains(text(), ${ADD_TO_CART_TEXT}[0])]

    Footer.Verify Footer In Polish Is Loaded

Verify MARS_BLACK Shop Page Is Loaded
    TopMenu.Verify Header In Polish Is Loaded

    #Header Text Verification
    Wait Until Page Contains    ${PAINT_TITLE}[3]

    @{expected_description_text}=    Create List     
    ...    Nasza podstawowa gama kolorów to:
    ...    11 żywych kolorów: odpowiednie dla początkujących lub jako dodatek do istniejącej kolekcji artysty. Każdy kolor jest projektowany indywidualnie, aby zapewnić optymalną ilość pigmentu dla każdej receptury.
    ...    Wysokiej jakości pigmenty zapewniają równomierne, nasycone kolory i doskonałe krycie, dzięki czemu kolory nie blakną z upływem czasu. Wszystkie używane przez nas pigmenty mają bardzo dobrą lub doskonałą odporność na światło.
    ...    Duże tubki o pojemności 120 ml są wystarczająco duże, aby pomieścić wystarczającą ilość farby do większości projektów lub obrazów
    ...    Spoiwo składa się w 100% z żywicy akrylowej. Farbę można łatwo rozcieńczyć wodą
    ...    Nasze farby są nietoksyczne i bezpieczne.
    ...    Jesteśmy producentem i z radością przyjmiemy opinie oraz sugestie dotyczące tworzenia nowych i ulepszania istniejących produktów.

    ${actual_description_full}=    Get Text    xpath=//*[@data-id='5bee1109']
    ${actual_description_text}=    Split String    ${actual_description_full}    \n    # Splits the text into a list of paragraphs
    Should Be Equal As Strings    ${actual_description_text}    ${expected_description_text}

    Wait Until Page Contains Element    xpath=//*[@data-id='31d45dce']//*[contains(text(), ${CURRENT_PRICE}[0])]
    Wait Until Page Contains Element    xpath=//*[@data-id='31d45dce']//*[contains(text(), ${CURRENT_CURRENCY}[0])]
    Wait Until Page Contains Element    xpath=//*[@name='add-to-cart' and contains(text(), ${ADD_TO_CART_TEXT}[0])]

    Footer.Verify Footer In Polish Is Loaded

Verify VIOLET Shop Page Is Loaded
    TopMenu.Verify Header In Polish Is Loaded

    #Header Text Verification
    Wait Until Page Contains    ${PAINT_TITLE}[6]

    @{expected_description_text}=    Create List     
    ...    Nasza podstawowa gama kolorów to:
    ...    11 żywych kolorów: odpowiednie dla początkujących lub jako dodatek do istniejącej kolekcji artysty. Każdy kolor jest projektowany indywidualnie, aby zapewnić optymalną ilość pigmentu dla każdej receptury.
    ...    Wysokiej jakości pigmenty zapewniają równomierne, nasycone kolory i doskonałe krycie, dzięki czemu kolory nie blakną z upływem czasu. Wszystkie używane przez nas pigmenty mają bardzo dobrą lub doskonałą odporność na światło.
    ...    Duże tubki o pojemności 120 ml są wystarczająco duże, aby pomieścić wystarczającą ilość farby do większości projektów lub obrazów
    ...    Spoiwo składa się w 100% z żywicy akrylowej. Farbę można łatwo rozcieńczyć wodą
    ...    Nasze farby są nietoksyczne i bezpieczne.
    ...    Jesteśmy producentem i z radością przyjmiemy opinie oraz sugestie dotyczące tworzenia nowych i ulepszania istniejących produktów.

    ${actual_description_full}=    Get Text    xpath=//*[@data-id='5bee1109']
    ${actual_description_text}=    Split String    ${actual_description_full}    \n    # Splits the text into a list of paragraphs
    Should Be Equal As Strings    ${actual_description_text}    ${expected_description_text}

    Wait Until Page Contains Element    xpath=//*[@data-id='31d45dce']//*[contains(text(), ${CURRENT_PRICE}[0])]
    Wait Until Page Contains Element    xpath=//*[@data-id='31d45dce']//*[contains(text(), ${CURRENT_CURRENCY}[0])]
    Wait Until Page Contains Element    xpath=//*[@name='add-to-cart' and contains(text(), ${ADD_TO_CART_TEXT}[0])]

    Footer.Verify Footer In Polish Is Loaded

Verify OWN_SET Shop Page Is Loaded
    TopMenu.Verify Header In Polish Is Loaded

    #Header Text Verification
    Wait Until Page Contains    ${PAINT_TITLE}[0]

    @{expected_description_text}=    Create List     
    ...    Nasza podstawowa gama kolorów to:
    ...    11 żywych kolorów: odpowiednie dla początkujących lub jako dodatek do istniejącej kolekcji artysty. Każdy kolor jest projektowany indywidualnie, aby zapewnić optymalną ilość pigmentu dla każdej receptury.
    ...    Wysokiej jakości pigmenty zapewniają równomierne, nasycone kolory i doskonałe krycie, dzięki czemu kolory nie blakną z upływem czasu. Wszystkie używane przez nas pigmenty mają bardzo dobrą lub doskonałą odporność na światło.
    ...    Duże tubki o pojemności 120 ml są wystarczająco duże, aby pomieścić wystarczającą ilość farby do większości projektów lub obrazów
    ...    Spoiwo składa się w 100% z żywicy akrylowej. Farbę można łatwo rozcieńczyć wodą
    ...    Nasze farby są nietoksyczne i bezpieczne.
    ...    Jesteśmy producentem i z radością przyjmiemy opinie oraz sugestie dotyczące tworzenia nowych i ulepszania istniejących produktów.

    ${actual_description_full}=    Get Text    xpath=//*[@data-id='5bee1109']
    ${actual_description_text}=    Split String    ${actual_description_full}    \n    # Splits the text into a list of paragraphs
    Should Be Equal As Strings    ${actual_description_text}    ${expected_description_text}

    Wait Until Page Contains Element    xpath=//*[@data-id='31d45dce']//*[contains(text(), ${CURRENT_PRICE}[0])]
    Wait Until Page Contains Element    xpath=//*[@data-id='31d45dce']//*[contains(text(), ${CURRENT_CURRENCY}[0])]
    Wait Until Page Contains Element    xpath=//*[@name='add-to-cart' and contains(text(), ${ADD_TO_CART_TEXT}[0])]

    Footer.Verify Footer In Polish Is Loaded

Verify SET_6 Shop Page Is Loaded
    TopMenu.Verify Header In Polish Is Loaded

    #Header Text Verification
    Wait Until Page Contains    ${SET_TITLE}[1]

    @{expected_description_text}=    Create List     
    ...    Sześć najpopularniejszych kolorów podstawowych w jednym zestawie!

    ${actual_description_full}=    Get Text    xpath=//*[@data-id='5bee1109']
    ${actual_description_text}=    Split String    ${actual_description_full}    \n    # Splits the text into a list of paragraphs
    Should Be Equal As Strings    ${actual_description_text}    ${expected_description_text}

    Wait Until Page Contains Element    xpath=//*[@data-id='6364f8c6']//*[contains(text(), ${SET_PRICE}[0])]
    Wait Until Page Contains Element    xpath=//*[@data-id='6364f8c6']//*[contains(text(), ${CURRENT_CURRENCY}[0])]
    Wait Until Page Contains Element    xpath=//*[@name='add-to-cart' and contains(text(), ${ADD_TO_CART_TEXT}[0])]

    Footer.Verify Footer In Polish Is Loaded

Verify SET_12 Shop Page Is Loaded
    TopMenu.Verify Header In Polish Is Loaded

    #Header Text Verification
    Wait Until Page Contains    ${SET_TITLE}[2]

    @{expected_description_text}=    Create List     
    ...    Zestaw farb akrylowych FeelMyPaint 11 x 120 ml
    ...    Zestaw farb akrylowych w tubkach o pojemności 120 ml w 11 kolorach. Wszystkie nasze kolory naraz!
    ...    Kolory:
    ...    Tytanowa biel
    ...    Czarny Mars
    ...    Brązowy Mars
    ...    Żółty Mars
    ...    Phthalo Green
    ...    Ciepła zieleń
    ...    Niebieski ftalowy
    ...    Ultramaryna
    ...    Pirol Czerwony
    ...    Czerwony Mars
    ...    Żółty podstawowy
    ...    Fioletowa
    ...    POJEMNOŚĆ: 12 x 120 ml
    ...    Uwaga: produkt bez opakowania prezentowego

    ${actual_description_full}=    Get Text    xpath=//*[@data-id='5bee1109']
    ${actual_description_text}=    Split String    ${actual_description_full}    \n    # Splits the text into a list of paragraphs
    Should Be Equal As Strings    ${actual_description_text}    ${expected_description_text}

    Wait Until Page Contains Element    xpath=//*[@data-id='6364f8c6']//*[contains(text(), ${SET_PRICE}[1])]
    Wait Until Page Contains Element    xpath=//*[@data-id='6364f8c6']//*[contains(text(), ${CURRENT_CURRENCY}[0])]
    Wait Until Page Contains Element    xpath=//*[@name='add-to-cart' and contains(text(), ${ADD_TO_CART_TEXT}[0])]

    Footer.Verify Footer In Polish Is Loaded

Open SET_6 Page
    Click Element    xpath=//*[@href=${SHOP_LIST_SET_IMG}[1]]

Open SET_12 Page
    Click Element    xpath=//*[@href=${SHOP_LIST_SET_IMG}[2]]
