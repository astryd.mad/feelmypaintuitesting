*** Settings ***
Library        SeleniumLibrary
Library        String
Resource       TopMenu.robot
Resource       Footer.robot
Resource       Cookies.robot

*** Variables ***
${CONTACT}                    CONTACT
${YOUTUBE_ICON_CONTACT}                 xpath=//a[@href='https://www.youtube.com/@FeelMyPaint' and contains(@class, '55b9f41')]
${INSTAGRAM_ICON_CONTACT}               xpath=//a[(@href='https://www.instagram.com/feelmypaint_official/') and contains(@class, '6e200c8')]
${FACEBOOK_ICON_CONTACT}                xpath=//a[(@href='https://www.facebook.com/feelmypaintpl') and contains(@class, 'f61d088')]
${NAME_FIELD}        form-field-name
${EMAIL_FIELD}    form-field-email
${MESSAGE_FIELD}    form-field-message
${CAPCHA}        recaptcha-accessible-status
${SUBMIT_BUTTON}    xpath=//button[@type='submit']

*** Keywords ***

Accept All Cookies
    Cookies.Accept All Cookies

Verify Contact Page In Polish Loaded
    TopMenu.Verify Header In Polish Is Loaded
    Wait Until Page Contains                ${CONTACT}

    #Contact Description Text Verification
    @{expected_contact_description_text}=    Create List     
    ...    Jak można się z nami skontaktować:
    ...    Telefon: Przepraszamy, ale zwykle wszyscy pracownicy są zajęci procesem produkcyjnym, prosimy o napisanie do nas e-maila lub skorzystanie z formularza na tej stronie, a my skontaktujemy się z Tobą tak szybko, jak to możliwe (zwykle około 30 minut).
    ...    shop@feelmypaint.com – sklep i zamówienia
    ...    jerzy@feelmypaint.com – kwestie jakości i współpracy
    ...    info@feelmypaint.com – wszelkie pytania
    ...    Zachęcamy do kontaktu. Jesteśmy tutaj, aby Ci pomóc, udzielić informacji o naszych produktach i usługach.
    ...    Twoje opinie, zapytania, a nawet własne doświadczenia artystyczne są bardzo cenne i czekamy na wiadomość od Ciebie.
    ...    Dziękujemy za uznanie FeelMyPaint za źródło inspiracji i wsparcia dla Twojej artystycznej podróży. Nie możemy się doczekać, aby nawiązać z Tobą kontakt
    ...    Śledź nas również w sieciach społecznościowych:

    ${actual_contact_description_text}=    Get Text    xpath=//*[@data-id='4b1be74']
    ${actual_contact_description_text}=    Split String    ${actual_contact_description_text}    \n    # Splits the text into a list of paragraphs
    Should Be Equal As Strings    ${actual_contact_description_text}    ${expected_contact_description_text}

    Wait Until Page Contains Element    ${YOUTUBE_ICON_CONTACT}
    Wait Until Page Contains Element    ${INSTAGRAM_ICON_CONTACT}
    Wait Until Page Contains Element    ${FACEBOOK_ICON_CONTACT}
    Wait Until Page Contains Element    ${NAME_FIELD}
    Wait Until Page Contains Element    ${EMAIL_FIELD}
    Wait Until Page Contains Element    ${MESSAGE_FIELD}
    # Wait Until Page Contains Element    ${CAPCHA}
    Wait Until Page Contains Element    ${SUBMIT_BUTTON}

    Footer.Verify Footer In Polish Is Loaded