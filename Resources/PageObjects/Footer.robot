*** Settings ***
Library        SeleniumLibrary

*** Variables ***
${MY_ACCOUNT}                   xpath=//*[@id='menu-1-3368bae']//a[(@href='https://feelmypaint.com/my-account/') and text()='Moje konto']
@{MY_ACCOUNT_TEXT}              Moje konto    My Account
${CHECKOUT}                     xpath=//*[@id='menu-1-3368bae']//a[(@href='https://feelmypaint.com/checkout/') and text()='Złożenie zamówienia']
@{CHECKOUT_TEXT}                Złożenie zamówienia    Placing an order
${CART}                         xpath=//*[@id='menu-1-3368bae']//a[(@href='https://feelmypaint.com/cart/') and text()='Koszyk']
@{CART_TEXT}                    Koszyk    Basket
${SHOP}                         xpath=//*[@id='menu-1-3368bae']//a[(@href='https://feelmypaint.com/shop/') and text()='Sklep']
@{SHOP_TEXT}                    Sklep    shop
${PRIVACY_POLISY}               xpath=//*[@id='menu-1-065e6e6']//a[(@href='https://feelmypaint.com/polityka-prywatnosci/') and text()='Polityka prywatności']
@{PRIVACY_POLISY_TEXT}          Polityka prywatności    Privacy Policy
${STORE_REGULATIONS}            xpath=//*[@id='menu-1-065e6e6']//a[(@href='https://feelmypaint.com/regulamin-sklepu/') and text()='Regulamin Sklepu']
@{STORE_REGULATIONS_TEXT}       Regulamin Sklepu    Store Regulations
${TRANSPORTATION_DELIVERY}      xpath=//*[@id='menu-1-065e6e6']//a[(@href='https://feelmypaint.com/koszty-transportu-i-czas-dostawy/') and text()='Koszty transportu i czas dostawy']
@{TRANSPORTATION_DELIVERY_TEXT}          Koszty transportu i czas dostawy    Transportation costs and delivery time
${PAYMENTS}                     xpath=//*[@id='menu-1-065e6e6']//a[(@href='https://feelmypaint.com/metody-platnosci/') and text()='Metody płatności']
@{PAYMENTS_TEXT}              Metody płatności    Payment methods
${COMPLAINTS_RETURNS}           xpath=//*[@id='menu-1-065e6e6']//a[(@href='https://feelmypaint.com/reklamacje-i-zwroty/') and text()='Reklamacje i zwroty']
@{COMPLAINTS_RETURNS_TEXT}              Reklamacje i zwrot    Complaints and returns
${FOOTER_FMP_LOGO}              xpath=*//img[contains(@class, 'wp-image-73')]
${COMPANY_NAME}                 FeelMyPaint sp. z o.o., Poland
${COMPANY_EMAIL}                E-mail: info@feelmypaint.com
${COMPANY_NIP}                  NIP: 6783199623
${YOUTUBE_ICON_FOOTER}                 xpath=//a[@href='https://www.youtube.com/@FeelMyPaint' and contains(@class, 'c90f069')]
${INSTAGRAM_ICON_FOOTER}               xpath=//a[(@href='https://www.instagram.com/feelmypaint_official/') and contains(@class, 'a90b5a4')]
${FACEBOOK_ICON_FOOTER}                xpath=//a[(@href='https://www.facebook.com/feelmypaintpl') and contains(@class, 'e49f77d')]


*** Keywords ***
Verify Footer In Polish Is Loaded
    Wait Until Page Contains Element    ${MY_ACCOUNT}
    Wait Until Page Contains Element    ${CHECKOUT}
    Wait Until Page Contains Element    ${CART}
    Wait Until Page Contains Element    ${SHOP}
    Wait Until Page Contains Element    ${PRIVACY_POLISY} 
    Wait Until Page Contains Element    ${STORE_REGULATIONS}
    Wait Until Page Contains Element    ${TRANSPORTATION_DELIVERY}
    Wait Until Page Contains Element    ${PAYMENTS}
    Wait Until Page Contains Element    ${COMPLAINTS_RETURNS}
    Wait Until Page Contains Element    ${FOOTER_FMP_LOGO}
    Wait Until Page Contains            ${COMPANY_NAME}
    Wait Until Page Contains            ${COMPANY_EMAIL}
    Wait Until Page Contains            ${COMPANY_NIP}
    Wait Until Page Contains Element    ${YOUTUBE_ICON_FOOTER}
    Wait Until Page Contains Element    ${INSTAGRAM_ICON_FOOTER}
    Wait Until Page Contains Element    ${FACEBOOK_ICON_FOOTER}

Verify Footer In English Is Loaded
    Wait Until Page Contains Element    xpath=//a[contains(@href, 'my-account') and contains(text(), '${MY_ACCOUNT_TEXT}[1]')]
    Wait Until Page Contains Element    xpath=//a[contains(@href, 'checkout') and contains(text(), '${CHECKOUT_TEXT}[1]')]
    Wait Until Page Contains Element    xpath=//a[contains(@href, 'cart') and contains(text(), '${CART_TEXT}[1]')]
    Wait Until Page Contains Element    xpath=//a[contains(@href, 'shop') and contains(text(), '${SHOP_TEXT}[1]')]
    Wait Until Page Contains Element    xpath=//a[contains(@href, 'privacy-policy') and contains(text(), '${PRIVACY_POLISY_TEXT}[1]')]
    Wait Until Page Contains Element    xpath=//a[contains(@href, 'store-regulations') and contains(text(), '${STORE_REGULATIONS_TEXT}[1]')]
    Wait Until Page Contains Element    xpath=//a[contains(@href, 'transportation-costs-and-delivery-time') and contains(text(), '${TRANSPORTATION_DELIVERY_TEXT}[1]')]
    Wait Until Page Contains Element    xpath=//a[contains(@href, 'payment-methods') and contains(text(), '${PAYMENTS_TEXT}[1]')]
    Wait Until Page Contains Element    xpath=//a[contains(@href, 'complaints-and-returns') and contains(text(), '${COMPLAINTS_RETURNS_TEXT}[1]')]
    Wait Until Page Contains Element    ${FOOTER_FMP_LOGO}
    Wait Until Page Contains            ${COMPANY_NAME}
    Wait Until Page Contains            ${COMPANY_EMAIL}
    Wait Until Page Contains            ${COMPANY_NIP}
    Wait Until Page Contains Element    ${YOUTUBE_ICON_FOOTER}
    Wait Until Page Contains Element    ${INSTAGRAM_ICON_FOOTER}
    Wait Until Page Contains Element    ${FACEBOOK_ICON_FOOTER}


    