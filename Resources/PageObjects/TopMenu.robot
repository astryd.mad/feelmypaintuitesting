*** Settings ***
Library        SeleniumLibrary

*** Variables ***
${TOP_LOGO}                      xpath=//*[@data-id='305bbae']//img[contains(@src, 'https://feelmypaint.com/wp-content/uploads/2023/11/logologo.png')]
${POLISH_LANGUAGE_SELECTOR}      xpath=*//span[@lang="pl"]
${GERMAN_LANGUAGE_SELECTOR}      xpath=*//span[@lang="de"]
${ENGLISH_LANGUAGE_SELECTOR}     xpath=*//span[@lang="en"]
@{PAINTS_BUTTON_URL}             https://feelmypaint.com/paints/    https://feelmypaint.com/en/paints/
@{PAINTS_BUTTON_TEXT}            farby    paints
@{ABOUT_BUTTON_URL}              https://feelmypaint.com/about/    https://feelmypaint.com/en/about/
@{ABOUT_BUTTON_TEXT}             o nas    about
@{HOW_BUTTON_URL}                https://feelmypaint.com/how/    https://feelmypaint.com/en/how/
@{HOW_BUTTON_TEXT}               jak    how
@{SHOP_BUTTON_URL}               https://feelmypaint.com/shop/    https://feelmypaint.com/en/shop-2/
@{SHOP_BUTTON_TEXT}              sklep    shop
@{CONTACT_BUTTON_URL}            https://feelmypaint.com/contact/    https://feelmypaint.com/en/contact/
@{CONTACT_BUTTON_TEXT}           kontakt    contact
${LANGUAGE_DROPDOWN_PL}          xpath=*//li[contains(@class, 'menu-item') and contains(@class, 'wpml-ls-slot-4') and contains(@class, 'wpml-ls-item') and contains(@class, 'wpml-ls-item-pl') and contains(@class, 'wpml-ls-current-language') and contains(@class, 'wpml-ls-menu-item') and contains(@class, 'wpml-ls-last-item') and contains(@class, 'menu-item-type-wpml_ls_menu_item') and contains(@class, 'menu-item-object-wpml_ls_menu_item') and contains(@class, 'menu-item-has-children')]
${LANGUAGE_DROPDOWN_EN}          xpath=*//li[contains(@class, 'menu-item') and contains(@class, 'wpml-ls-slot-4') and contains(@class, 'wpml-ls-item') and contains(@class, 'wpml-ls-item-en') and contains(@class, 'wpml-ls-current-language') and contains(@class, 'wpml-ls-menu-item') and contains(@class, 'menu-item-type-wpml_ls_menu_item') and contains(@class, 'menu-item-object-wpml_ls_menu_item') and contains(@class, 'menu-item-has-children')]
${CART_BUTTON}                   xpath=//*[@data-id='837d1c5']

*** Keywords ***

Verify Header In Polish Is Loaded
    Page Should Contain Element            ${TOP_LOGO}
    Page Should Contain Element            xpath=//a[contains(@href, 'paints') and contains(text(), '${PAINTS_BUTTON_TEXT}[0]')]
    Page Should Contain Element            xpath=//a[contains(@href, 'about') and contains(text(), '${ABOUT_BUTTON_TEXT}[0]')]
    Page Should Contain Element            xpath=//a[contains(@href, 'how') and contains(text(), '${HOW_BUTTON_TEXT}[0]')]
    Page Should Contain Element            xpath=//a[contains(@href, 'shop') and contains(text(), '${SHOP_BUTTON_TEXT}[0]')]
    Page Should Contain Element            xpath=//a[contains(@href, 'contact') and contains(text(), '${CONTACT_BUTTON_TEXT}[0]')]
    Page Should Contain Element            ${LANGUAGE_DROPDOWN_PL}
    Page Should Contain Element            ${CART_BUTTON}

Verify Header In English Is Loaded
    Page Should Contain Element            ${TOP_LOGO}
    Page Should Contain Element            xpath=//a[contains(@href, 'paints') and contains(text(), '${PAINTS_BUTTON_TEXT}[1]')]
    Page Should Contain Element            xpath=//a[contains(@href, 'about') and contains(text(), '${ABOUT_BUTTON_TEXT}[1]')]
    Page Should Contain Element            xpath=//a[contains(@href, 'how') and contains(text(), '${HOW_BUTTON_TEXT}[1]')]
    Page Should Contain Element            xpath=//a[contains(@href, 'shop') and contains(text(), '${SHOP_BUTTON_TEXT}[1]')]
    Page Should Contain Element            xpath=//a[contains(@href, 'contact') and contains(text(), '${CONTACT_BUTTON_TEXT}[1]')]
    Page Should Contain Element            ${LANGUAGE_DROPDOWN_EN}
    Page Should Contain Element            ${CART_BUTTON}

# Select Polish Language
#     Wait Until Page Contains Element    ${LANGUAGE_DROPDOWN}
#     Mouse Over    ${LANGUAGE_DROPDOWN}
#     Sleep    1s
#     Click Element    ${ENGLISH_LANGUAGE_SELECTOR}
#     Sleep    5s

Go To Paints Page
    Click Element     xpath=//a[contains(@href, 'paints')]

Go To About Page
    Click Element     xpath=//a[contains(@href, 'about')]

Go To How Page
    Click Element     xpath=//a[contains(@href, 'how')]

Go To Shop Page
    Click Element     xpath=//a[contains(@href, 'shop')]

Go To Contact Page
    Click Element     xpath=//a[contains(@href, 'contact')]