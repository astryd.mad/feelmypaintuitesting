*** Settings ***
Library        SeleniumLibrary
Library        String

*** Variables ***
# ${SHOP_PAGE_BANNER}                xpath=//*[@data-id='bf6cbea']
# ${DELIVERY_ICON}                   xpath=//*[@data-id='04910e8']//img[contains(@src, 'https://feelmypaint.com/wp-content/uploads/2023/11/aboutus-icons-1.png')]
# ${RETURN_ICON}                     xpath=//*[@data-id='b2fb446']//img[contains(@src, 'https://feelmypaint.com/wp-content/uploads/2023/11/aboutus-icons-2.png')]

*** Keywords ***
Accept All Cookies
    Wait Until Page Contains Element        hu-cookies-save
    Click Element        hu-cookies-save
