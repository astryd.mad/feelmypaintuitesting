*** Settings ***
Library        SeleniumLibrary

*** Variables ***
${PHTALO_GREEN_AMAZON_URL_ID}         B0CC5WP6G9
${PIROLLE_RED_AMAZON_URL_ID}         B0CY5T1P4V
${BROWN_OXIDE_AMAZON_URL_ID}         B0CY5Q4YJ2
${TITANIUM_WHITE_AMAZON_URL_ID}         B0CY6GH5WJ
${YELLOW_OXIDE_AMAZON_URL_ID}         B0CKRMWSXR
${PHTALO_BLUE_AMAZON_URL_ID}         B0CKRKYZ8H
${MARS_RED_AMAZON_URL_ID}         B0CKRMGDBD
${WARM_GREEN_AMAZON_URL_ID}         B0CJFDNZDR
${ULTRAMARINE_AMAZON_URL_ID}         B0CBT1YK4W
${PRIMARY_YELLOW_AMAZON_URL_ID}         B0CC62SBYX
${MARS_BLACK_AMAZON_URL_ID}         B0CXMB94HW
${VIOLET_AMAZON_URL_ID}         B0CVW6N5PP


*** Keywords ***
Verify Amazon PHTALO_GREEN Page Loaded
    ${CURRENT_URL}=  Get Location
    Should Contain  ${CURRENT_URL}  ${PHTALO_GREEN_AMAZON_URL_ID}

Verify Amazon PIROLLE_RED Page Loaded
    ${CURRENT_URL}=  Get Location
    Should Contain  ${CURRENT_URL}  ${PIROLLE_RED_AMAZON_URL_ID}

Verify Amazon BROWN_OXIDE Page Loaded
    ${CURRENT_URL}=  Get Location
    Should Contain  ${CURRENT_URL}  ${BROWN_OXIDE_AMAZON_URL_ID}

Verify Amazon TITANIUM_WHITE Page Loaded
    ${CURRENT_URL}=  Get Location
    Should Contain  ${CURRENT_URL}  ${TITANIUM_WHITE_AMAZON_URL_ID}

Verify Amazon YELLOW_OXIDE Page Loaded
    ${CURRENT_URL}=  Get Location
    Should Contain  ${CURRENT_URL}  ${YELLOW_OXIDE_AMAZON_URL_ID}

Verify Amazon PHTALO_BLUE Page Loaded
    ${CURRENT_URL}=  Get Location
    Should Contain  ${CURRENT_URL}  ${PHTALO_BLUE_AMAZON_URL_ID}

Verify Amazon MARS_RED Page Loaded
    ${CURRENT_URL}=  Get Location
    Should Contain  ${CURRENT_URL}  ${MARS_RED_AMAZON_URL_ID}

Verify Amazon WARM_GREEN Page Loaded
    ${CURRENT_URL}=  Get Location
    Should Contain  ${CURRENT_URL}  ${WARM_GREEN_AMAZON_URL_ID}

Verify Amazon ULTRAMARINE Page Loaded
    ${CURRENT_URL}=  Get Location
    Should Contain  ${CURRENT_URL}  ${ULTRAMARINE_AMAZON_URL_ID}

Verify Amazon PRIMARY_YELLOW Page Loaded
    ${CURRENT_URL}=  Get Location
    Should Contain  ${CURRENT_URL}  ${PRIMARY_YELLOW_AMAZON_URL_ID}

Verify Amazon MARS_BLACK Page Loaded
    ${CURRENT_URL}=  Get Location
    Should Contain  ${CURRENT_URL}  ${MARS_BLACK_AMAZON_URL_ID}

Verify Amazon VIOLET Page Loaded
    ${CURRENT_URL}=  Get Location
    Should Contain  ${CURRENT_URL}  ${VIOLET_AMAZON_URL_ID}

# Accept Amazon Cookies
#     Click Element    sp-cc-accept
