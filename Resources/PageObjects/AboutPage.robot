*** Settings ***
Library        SeleniumLibrary
Library        String
Resource       TopMenu.robot
Resource       Footer.robot

*** Variables ***
${ABOUT}                        ABOUT
${IN_TEXT_LOGO}                 xpath=//*[@data-id='fb71b5b']//img[contains(@src, 'https://feelmypaint.com/wp-content/uploads/2023/11/FMP_logo_black-2.png')]
${PACKAGE_IMAGE}                xpath=//*[@data-id='fb425ab']//img[contains(@src, 'https://feelmypaint.com/wp-content/uploads/2023/11/vfk1-2-113x300.png')]


*** Keywords ***
Verify About Page In Polish Loaded
    TopMenu.Verify Header In Polish Is Loaded
    Wait Until Page Contains                ${ABOUT}
    Wait Until Page Contains Element        ${IN_TEXT_LOGO}
    Wait Until Page Contains Element        ${PACKAGE_IMAGE}
    
    #Description Text Verification
    @{expected_description}=    Create List     
    ...    FeelMyPaint produkuje artystyczne farby akrylowe w Krakowie od 2022 roku.
    ...    Nasze farby są wytwarzane ręcznie w małych partiach, aby zapewnić jakość i świeżość. Używamy 100% żywicy akrylowej i odpornych na światło pigmentów o najbardziej zrównoważonej zawartości pigmentu i napełniacza dla każdego koloru.
    ...    Nasze produkty można kupić wyłącznie bezpośrednio od nas. Farby można zamawiać za pośrednictwem tej strony internetowej oraz na platformach Amazon i Allegro. Ponieważ nie prowadzimy dystrybucji za pośrednictwem sklepów, jesteśmy w stanie utrzymać nasze ceny na niskim poziomie, a osobisty kontakt z naszymi klientami oznacza, że możemy zaoferować doskonałe wsparcie techniczne, zawsze zapewniając najwyższą jakość naszych produktów.
    ...    Obecnie w naszej ofercie znajduje się 11 kolorów akrylowych, które pakujemy w tubki o pojemności 120 ml.

    ${actual_description_full}=    Get Text    xpath=//*[@data-id='d3c8583']
    ${actual_description}=    Split String    ${actual_description_full}    \n    # Splits the text into a list of paragraphs
    Should Be Equal As Strings    ${expected_description}    ${actual_description}

    Footer.Verify Footer In Polish Is Loaded
