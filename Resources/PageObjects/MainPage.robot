*** Settings ***
Library        SeleniumLibrary
Resource       TopMenu.robot
Resource       Footer.robot

*** Variables ***
${MAIN_PAGE_BANNER_IMAGE}         xpath=//img[contains(@src, 'https://feelmypaint.com/wp-content/uploads/2024/02/collage-2collage_normal')]
@{OUR_PAINTS}                     NASZE FARBY    OUR PAINTS
@{HOW_WE_DO_IT}                   JAK TO ROBIMY?    HOW DO WE DO IT?
${PAINTS_CARDS}                   xpath=*//div[contains(@data-element_type, 'container') and contains (@data-id, 'c17b7c7')]
@{MAIN_PAGE_SHOP_BUTTON_TEXT}               SKLEP    STORE


*** Keywords ***
Verify Main Page In Polish Loaded
    TopMenu.Verify Header In Polish Is Loaded
    Wait Until Page Contains Element        ${MAIN_PAGE_BANNER_IMAGE}
    Wait Until Page Contains                ${OUR_PAINTS}[0]
    Wait Until Page Contains                ${HOW_WE_DO_IT}[0]
    Wait Until Page Contains Element        ${PAINTS_CARDS}
    Wait Until Page Contains Element        xpath=//span[@class='elementor-button-text' and contains(text(), '${MAIN_PAGE_SHOP_BUTTON_TEXT}[0]')]
    Footer.Verify Footer In Polish Is Loaded

Verify Main Page In English Loaded
    TopMenu.Verify Header In English Is Loaded
    Wait Until Page Contains Element        ${MAIN_PAGE_BANNER_IMAGE}
    Wait Until Page Contains                ${OUR_PAINTS}[1]
    Wait Until Page Contains                ${HOW_WE_DO_IT}[1]
    Wait Until Page Contains Element        ${PAINTS_CARDS}
    Wait Until Page Contains Element        xpath=//span[@class='elementor-button-text' and contains(text(), '${MAIN_PAGE_SHOP_BUTTON_TEXT}[1]')]
    Footer.Verify Footer In English Is Loaded