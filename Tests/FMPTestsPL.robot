*** Settings ***
Documentation   This suite verifies all Feel My Paint site pages are opening
Resource    ../Resources/FeelMyPaint.robot
Resource    ../Resources/Common.robot
Suite Setup    Begin Test Suite
Suite Teardown    End Test Suite

#run command
#robot -d results tests/FMPTestsPL.robot

*** Variables ***
${BROWSER}                 chrome
${URL}                    https://feelmypaint.com/

*** Test Cases ***
Main Page Opens PL
   [Documentation]    This test verifies Main page in Polish opens
   [Tags]  Sanity
   FeelMyPaint.Open Main Page PL    ${URL}

Paints Page Opens PL
   [Documentation]    This test verifies Paints page in Polish opens
   [Tags]  Sanity
   FeelMyPaint.Open Paints Page PL    ${URL}

About Page Opens PL
   [Documentation]    This test verifies About page in Polish opens
   [Tags]  Sanity
   FeelMyPaint.Open About Page PL    ${URL}

How Page Opens PL 
   [Documentation]    This test verifies How page in Polish opens
   [Tags]  Sanity
   FeelMyPaint.Open How Page PL    ${URL}

Shop Page Opens PL 
   [Documentation]    This test verifies Shop page in Polish opens
   [Tags]  Sanity
   FeelMyPaint.Open Shop Page PL    ${URL}

Contact Page Opens PL 
   [Documentation]    This test verifies Shop page in Polish opens
   [Tags]  Sanity
   FeelMyPaint.Open Contact Page PL    ${URL}