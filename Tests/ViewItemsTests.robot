*** Settings ***
Documentation   This suite verifies all Feel My Paint site pages are opening
Resource    ../Resources/FeelMyPaint.robot
Resource    ../Resources/Common.robot
Resource    ../Resources/PageObjects/ShopPage.robot
Suite Setup    Begin Test Suite
Suite Teardown    End Test Suite

#run command
#robot -d results tests/ViewItemsTests.robot

*** Variables ***
${BROWSER}                 chrome
${URL} =                    https://feelmypaint.com/

*** Test Cases ***
User Can View Paint Details On Paints Page
   [Documentation]    This test verifies the user can view paints info on Paints page
   [Tags]  Smoke
   FeelMyPaint.Open Paints Page PL     ${URL}
   FeelMyPaint.Open PHTALO_GREEN Paint Details
   FeelMyPaint.Open PIROLLE_RED Paint Details
   FeelMyPaint.Open BROWN_OXIDE Paint Details
   FeelMyPaint.Open TITANIUM_WHITE Paint Details
   FeelMyPaint.Open YELLOW_OXIDE Paint Details
   FeelMyPaint.Open PHTALO_BLUE Paint Details
   FeelMyPaint.Open MARS_RED Paint Details
   FeelMyPaint.Open WARM_GREEN Paint Details
   FeelMyPaint.Open ULTRAMARINE Paint Details
   FeelMyPaint.Open PRIMARY_YELLOW Paint Details
   FeelMyPaint.Open MARS_BLACK Paint Details
   FeelMyPaint.Open VIOLET Paint Details

User Can Follow Shop Link From Item Details Page
   [Documentation]    This test verifies the user can go to paint page in the shop from paint popup in paints list
   [Tags]  Functional
   FeelMyPaint.Open Paints Page PL    ${URL}
   FeelMyPaint.Verify Shop Icon Navigates User To The Specific Paint Page From Paint Popup

User Can View Paints List In Shop
   [Documentation]    This test verifies the user can view paints list in shop
   [Tags]  Smoke
   FeelMyPaint.Open Paints List In The Shop    ${URL}

User Can Open Sets List In Shop
   [Documentation]    This test verifies the user can view sets list in shop
   [Tags]  Smoke
   FeelMyPaint.Open Sets List In The Shop    ${URL}

User Can Add Item To Cart From Paints Shop List
    [Documentation]    This test verifies the user can add one item to cart
    [Tags]  Smoke
    FeelMyPaint.Open Paints List In The Shop    ${URL}
    FeelMyPaint.Add One Paint Item To Cart

User Can Open Set Page From Sets Shop List
    [Documentation]    This test verifies the user can open set page in the shop
    [Tags]  Functional
    FeelMyPaint.Open Sets List In The Shop    ${URL}
    FeelMyPaint.Verify User Navigates To The Specific Set Page After Clicking On Set Tile In Sets List

User Can Follow Allegro Link From Item Details Page
   [Documentation]    This test verifies the user can go to Allegro item page from paint popup in paints list
   [Tags]  Functional
   FeelMyPaint.Open Paints Page PL    ${URL}
   FeelMyPaint.Verify Allegro Icon Navigates User To The Specific Allegro Page From Paint Popup

User Can Follow Amazon Link From Item Details Page
   [Documentation]    This test verifies the user can go to Amazon item page from paint popup in paints list
   [Tags]  Functional
   FeelMyPaint.Open Paints Page PL    ${URL}
   FeelMyPaint.Verify Amazon Icon Navigates User To The Specific Amazon Page From Paint Popup
   FeelMyPaint.Go To Previous Page
