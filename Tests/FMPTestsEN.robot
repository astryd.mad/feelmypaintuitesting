*** Settings ***
Documentation   This suite verifies all Feel My Paint site pages are opening
Resource    ../Resources/FeelMyPaint.robot
Resource    ../Resources/Common.robot
Suite Setup    Begin Test Suite
Suite Teardown    End Test Suite

#run command
#robot -d results tests/FMPTestsEN.robot

*** Variables ***
${BROWSER}                 chrome
${URL}                     https://feelmypaint.com/en/

*** Test Cases ***
Main Page Opens EN
   [Documentation]    This test verifies Main page in English opens
   [Tags]  Sanity
   FeelMyPaint.Open Main Page EN    ${URL}

Paints Page Opens EN
   [Documentation]    This test verifies Paints page in English opens
   [Tags]  Sanity
   FeelMyPaint.Open Paints Page EN    ${URL}

# About Page Opens EN
#    [Documentation]    This test verifies About page in English opens
#    [Tags]  Sanity
#    FeelMyPaint.Open About Page EN    ${URL}[1]

# How Page Opens EN 
#    [Documentation]    This test verifies How page in English opens
#    [Tags]  Sanity
#    FeelMyPaint.Open How Page EN    ${URL}[1]

# Shop Page Opens EN 
#    [Documentation]    This test verifies Shop page in English opens
#    [Tags]  Sanity
#    FeelMyPaint.Open Shop Page EN    ${URL}[1]

# Contact Page Opens EN 
#    [Documentation]    This test verifies Shop page in English opens
#    [Tags]  Sanity
#    FeelMyPaint.Open Contact Page EN    ${URL}[1]